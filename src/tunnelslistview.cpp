#include "constants.h"
#include "settings.h"
#include "tunnelslistview.h"
#include "tunnelitemdelegate.h"
#include "tunnelsmanager.h"

struct TunnelsListView::Private
{
    bool handleDrop = false;
};

TunnelsListView::TunnelsListView(QWidget *parent)
    : QListView(parent)
{
    p = new Private;

    setSelectionMode(QAbstractItemView::SingleSelection);
    setDragEnabled(true);
    setAcceptDrops(true);
    setDragDropMode(QAbstractItemView::InternalMove);
    // setDropIndicatorShown(true);
    // showDropIndicator();
    // setDefaultDropAction(Qt::MoveAction);

    auto model = new QStandardItemModel(0, 1, this);
    setModel(model);

    setItemDelegate(new TunnelItemDelegate(this));
    setEditTriggers(QAbstractItemView::NoEditTriggers);

    auto tm = TunnelsManager::instance();
    connect(tm, SIGNAL(reloaded()), this, SLOT(reload()));
    connect(model, SIGNAL(layoutChanged(const QList<QPersistentModelIndex> &, QAbstractItemModel::LayoutChangeHint)),
        this, SLOT(modelLayoutChanged(const QList<QPersistentModelIndex> &, QAbstractItemModel::LayoutChangeHint)));
    connect(model, SIGNAL(rowsRemoved(const QModelIndex &, int, int)),
        this, SLOT(modelRowsRemoved(const QModelIndex &, int, int)));
}

TunnelsListView::~TunnelsListView()
{
    delete p;
}


void TunnelsListView::reload()
{
    auto model = this->model();
    QStandardItemModel * sModel = qobject_cast<QStandardItemModel*>(model);

    // preserve selection
    auto selectedGuid = selectedTunnelGuid();

    // remove all rows
    model->removeRows(0, model->rowCount());

    int row = 0;
    auto tm = TunnelsManager::instance();
    QModelIndex selectedIndex;

    // auto tunnels = tm->tunnels();
    // for (const auto & t : tunnels) {
    //     model->insertRows(row, 1);
    //     auto index = model->index(row, 0);
    //     QStandardItem * item = sModel->itemFromIndex(index);
    //     item->setDropEnabled(false);
        
    //     if (t.guid == selectedGuid) {
    //         selectedIndex = index;
    //     }

    //     if (!model->setData(index, t.guid)) {
    //         qWarning() << "failed to set data for row" << row;
    //     }
    //     row++;
    // }

    auto tunnels = tm->tunnels();
    for (const auto & t : tunnels) {
        model->insertRows(row, 1);
        auto index = model->index(row, 0);
        QStandardItem * item = sModel->itemFromIndex(index);
        item->setDropEnabled(false);
        
        if (t.guid == selectedGuid) {
            selectedIndex = index;
        }

        if (!model->setData(index, t.guid)) {
            qWarning() << "failed to set data for row" << row;
        }
        row++;
    }

    setCurrentIndex(selectedIndex);
    // qDebug() << "[DEBUG]" << "tunnels list reloaded";
    // emit reloaded();
}

QString TunnelsListView::selectedTunnelGuid()
{
    auto indexes = selectedIndexes();

    if (indexes.isEmpty()) {
        return QString();
    }

    auto tm = TunnelsManager::instance();

    auto index = indexes.first();
    auto tunnelGuid = index.data().toString();
    auto tunnel = tm->tunnel(tunnelGuid);

    return tunnel.guid;
}


void TunnelsListView::modelLayoutChanged(const QList<QPersistentModelIndex> &, QAbstractItemModel::LayoutChangeHint hint)
{
    p->handleDrop = true;
}


void TunnelsListView::modelRowsRemoved(const QModelIndex &parent, int first, int last)
{
    if (!p->handleDrop) {
        return;
    }

    p->handleDrop = false;

    auto model = this->model();
    auto tm = TunnelsManager::instance();
    QStringList newOrder;

    for(int r = 0; r < model->rowCount(); ++r) {
        auto index = model->index(r, 0);
        newOrder << index.data().toString();
        // auto tunnel = tm->tunnel(tunnelGuid);

        // qDebug() << tunnel.name << index.isValid();
    }

    tm->reorderTunnels(newOrder);
}