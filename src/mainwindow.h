#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore>
#include <QMainWindow>
#include <QUrl>

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    static MainWindow * instance();
    ~MainWindow();
    void changeEvent(QEvent *event);
    void forceShow();

    /*
     * Raise main window and show modal error message box
     */
    void showErrorMessageBox(const QString & msg);

public slots:
    void configChanged();

protected:
    void hideEvent(QHideEvent *event);
    void showEvent(QShowEvent *event);
    void moveEvent(QMoveEvent * event);
    void resizeEvent(QResizeEvent * event);
    void retranslateUi();

private slots:
    void createTunnel();
    void editTunnel();
    void copyTunnel();
    void deleteTunnel();
    void startTunnel();
    void restartTunnel();
    void stopTunnel();
    void openEventLog();
    void quit();
    void tunnelsSelectionChanged(const QItemSelection &, const QItemSelection &);
    void helpMenuTriggered();
    void openPreferences();
    void rowDoubleClicked(const QModelIndex &index);


private:
    MainWindow();
    void rememberGeometryAndState();
    struct Private;
    Private * p;
};

#endif