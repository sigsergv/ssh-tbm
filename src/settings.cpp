#include <QtWidgets>

#include "utils.h"
#include "settings.h"
#include "traymanager.h"
#include "mainwindow.h"

namespace Settings
{

static QSettings * instance = 0;
static QTranslator * translatorInst = 0;

static const QStringList allowedTunnelKeys = {"name", "notes", "serveraddress", "serverport", "localport", "kind", 
    "remotehost", "localhost", "remoteport", "authtype", "keyfile", "username"};


QSettings * settings()
{
    if (instance == 0) {

        QCoreApplication::setApplicationName("ssh-tbm");

        QCoreApplication::setApplicationVersion(SSHTBM_VERSION);
        QCoreApplication::setOrganizationName("regolit.com");
        QCoreApplication::setOrganizationDomain("ssh-tbm.regolit.com");

        dataPath();  // call here to create directory
        QString path = settingsPath();
        if (path.isEmpty()) {
            return 0;
        }
#ifdef QT_DEBUG
        instance = new QSettings(path + "/settings-debug.ini", QSettings::IniFormat);
#else
        instance = new QSettings(path + "/settings.ini", QSettings::IniFormat);
#endif
    }

    return instance;
}

QTranslator * translator()
{
    if (translatorInst == 0) {
        translatorInst = new QTranslator();
        if (!translatorInst->load(":/translators/" + getTranslationLanguage() + ".qm")) {
            qDebug() << "Failed to load translations file, using default EN translation";
        };
        QCoreApplication::installTranslator(translatorInst);
    }
    return translatorInst;
}

void ensurePathExists(const QString & path)
{
    QFileInfo fi(path);
    if (fi.isDir()) {
        return;
    }

    QDir dir;

    if (!fi.exists()) {
        dir.mkpath(path);
        return;
    }

    if (!fi.isDir()) {
        QMessageBox::critical(0, "critical", "Required file path is busy by something else: " + path);
        QCoreApplication::quit();
        return;
    }
}

#ifdef Q_OS_OSX
#define SETTINGS_DIR "Library/Preferences/ssh-tbm"
#define DATA_DIR "Library/Application Support/ssh-tbm"
#endif

#ifdef Q_OS_LINUX
#define SETTINGS_DIR ".config/ssh-tbm"
#define DATA_DIR ".local/share/ssh-tbm"
#endif

#ifdef Q_OS_WIN32
#define SETTINGS_DIR "Local Settings/ssh-tbm"
#define DATA_DIR "Application Data/ssh-tbm"
#endif

static const QString SETTINGS_PATH(QDir::homePath() + "/" +  SETTINGS_DIR);
static const QString DATA_PATH(QDir::homePath() + "/" +  DATA_DIR);

#ifdef Q_OS_OSX
static const QString MACOS_LAUNCHD_PLIST_PATH(QDir::homePath() + "/Library/LaunchAgents/com.regolit.ssh-tbm.plist");
#endif

#ifdef Q_OS_LINUX
static const QString LINUX_AUTOSTART_DESKTOP_PATH(QDir::homePath() + "/.config/autostart/com.regolit.ssh-tbm.desktop");
#endif

/*
 * Path to directory where application settings are stored.
 *
 * linux: ~/.local/config/ssh-tbm
 * macos: ~/Library/Preferences/ssh-tbm
 */
QString settingsPath()
{
    ensurePathExists(SETTINGS_PATH);
    return SETTINGS_PATH;
}

QString dataPath()
{
    ensurePathExists(DATA_PATH);
    return DATA_PATH;
}

QString eventlogPath()
{
    auto value = dataPath() + "/ssh-tbm-eventlog.txt";
    return value;
}

// QString controlSocketPath()
// {
//     auto value = dataPath() + "/ctl";
//     ensurePathExists(value);
//     return value;
// }

QString homePath()
{
    return QStandardPaths::locate(QStandardPaths::HomeLocation, QString(), QStandardPaths::LocateDirectory);
}

QString binaryPath(const ComponentBin kind)
{
#ifdef Q_OS_OSX
    switch (kind) {
    case SshBin:
        return "/usr/bin/ssh";

    case SshAskPassBin: {
        auto fi = QFileInfo(QCoreApplication::applicationFilePath());
        auto dir = fi.absoluteDir();
        dir.cd("../Resources");
        return dir.absolutePath() + "/mac-ssh-askpass";
        };
    }
#endif

#ifdef Q_OS_LINUX
    switch (kind) {
    case SshBin:
        return "/usr/bin/ssh";
    case SshAskPassBin:
        return "/usr/bin/ssh-askpass";
    }
#endif

#ifdef Q_OS_WIN32
    auto fi = QFileInfo(QCoreApplication::applicationFilePath());
    auto dir = fi.absoluteDir();

    switch (kind) {
    case SshBin:
        return dir.absolutePath() + "/bundled-ssh/ssh.exe";

    case SshAskPassBin: {
        return dir.absolutePath() + "/ssh-askpass.exe";
        };
    }

#endif

    return QString();
}

// left for tunnels migration from version 1 to 2
QList<QMap<QString, QVariant> > tunnels()
{
    QList<QMap<QString, QVariant> > tunnels;
    auto s = settings();

    s->beginGroup("tunnels");
    QStringList tunnelsOrder = s->value("order", QStringList()).toStringList();
    tunnelsOrder.removeDuplicates();

    for (const auto tunnelGuid : tunnelsOrder) {
        QMap<QString, QVariant> t;
        s->beginGroup(tunnelGuid);
        for (const auto key : s->childKeys()) {
            t[key] = s->value(key);
        }

        if (t.contains("name")) {
            t["guid"] = tunnelGuid;
            tunnels << t;
        }
        s->endGroup();  // tunnelGuid
        
    }
    s->endGroup(); // "tunnels"

    return tunnels;
}

bool getApplicationAutostart()
{
#ifdef Q_OS_OSX
    QFileInfo pfi(MACOS_LAUNCHD_PLIST_PATH);
    return pfi.exists();
#endif

#ifdef Q_OS_LINUX
    QFileInfo pfi(LINUX_AUTOSTART_DESKTOP_PATH);
    return pfi.exists();
#endif
}

void setApplicationAutostart(bool state)
{
#ifdef Q_OS_OSX
    // see https://stackoverflow.com/questions/3358410/programmatically-run-at-startup-on-mac-os-x
    QFile pf(MACOS_LAUNCHD_PLIST_PATH);

    if (state) {
        if (pf.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QFile r(":/com.regolit.ssh-tbm.plist");
            r.open(QIODevice::ReadOnly | QIODevice::Text);
            auto tpl = QString::fromLatin1(r.readAll());
            auto data = tpl.arg(QCoreApplication::applicationFilePath());
            pf.write(data.toLatin1());

            // now we need to execute "launchctl load -w FILENAME"
            QString program = "/bin/launchctl";
            QStringList arguments;
            arguments << "load" << "-w" << MACOS_LAUNCHD_PLIST_PATH;
            QProcess process;
            process.start(program, arguments);
            if (!process.waitForFinished()) {
                qDebug() << "launchctl failed: " << process.errorString();
            }
        }
    } else {
        pf.remove();
        // now we need to execute "launchctl unload -w FILENAME"
        QString program = "/bin/launchctl";
        QStringList arguments;
        arguments << "unload" << "-w" << MACOS_LAUNCHD_PLIST_PATH;
        QProcess process;
        process.start(program, arguments);
        if (!process.waitForFinished()) {
            qDebug() << "launchctl failed: " << process.errorString();
        }
    }
#endif

#ifdef Q_OS_LINUX
    QFileInfo pfi(LINUX_AUTOSTART_DESKTOP_PATH);
    auto dir = pfi.absoluteDir();
    dir.mkpath(".");

    QFile pf(LINUX_AUTOSTART_DESKTOP_PATH);
    if (state) {
        if (pf.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QFile r(":/com.regolit.ssh-tbm.desktop");
            r.open(QIODevice::ReadOnly | QIODevice::Text);
            pf.write(r.readAll());
        }
    } else {
        pf.remove();
    }
#endif
}

static const QStringList ALLOWED_LANGUAGES = {"en", "ru"};

QString getTranslationLanguage()
{
    auto s = settings();

    s->beginGroup("ui");
    auto lang = s->value("language", "en").toString();
    if (!ALLOWED_LANGUAGES.contains(lang)) {
        lang = "en";
    }
    s->endGroup();

    return lang;
}

void setTranslationLanguage(const QString newLang)
{
    auto lang = newLang;
    if (!ALLOWED_LANGUAGES.contains(lang)) {
        lang = "en";
    }

    auto s = settings();

    s->beginGroup("ui");
    s->setValue("language", lang);
    s->endGroup();

    auto t = Settings::translator();
    if (!t->load(":/translators/"+ lang +".qm")) {
        qDebug() << "Failed to load translations file, using default EN translation";
    }
}


bool getApplicationTrayEnabled()
{
    auto s = settings();

    s->beginGroup("ui");
    auto value = s->value("tray_icon", false).toBool();
    s->endGroup();

    return value;
}

void setApplicationTrayEnabled(bool value)
{
    auto s = settings();

    s->beginGroup("ui");
    s->setValue("tray_icon", value);
    s->endGroup();

    TrayManager * tray = TrayManager::instance();
    tray->configChanged();
}


bool getEventLogEnabled()
{
    auto s = settings();

    s->beginGroup("ui");
    auto value = s->value("event_log", false).toBool();
    s->endGroup();

    return value;
}

void setEventLogEnabled(bool value)
{
    auto s = settings();

    s->beginGroup("ui");
    s->setValue("event_log", value);
    s->endGroup();

    auto mw = MainWindow::instance();

    if (mw != 0) {
        mw->configChanged();
    }
}


}