#include "aboutdialog.h"
#include "settings.h"
#include "ui_aboutdialog.h"


struct AboutDialog::Private
{
    Ui::AboutDialog ui;
};

AboutDialog::AboutDialog(QWidget *parent, Qt::WindowFlags f)
    : QDialog(parent, f)
{
    p = new Private;
    p->ui.setupUi(this);

    p->ui.versionLabel->setText(SSHTBM_VERSION);
}


AboutDialog::~AboutDialog()
{
    delete p;
}