#ifndef TUNNELSMANAGER_H
#define TUNNELSMANAGER_H

#include <QtCore>

/*
 * TunnelsManager provides interface to tunnel management: list, add, update, start, stop.
 *
 * It also provides signals that emit when tunnel(s) state is changed.
 *
 * It incapsulates all operations, any other components must not directly access Settings to 
 * store or query tunnels data. Instead they must subscribe to signals that TunnelsManager emit.
 */



class TunnelsManager : public QObject
{
    Q_OBJECT
public:
    // NEW
    struct TunnelConnection {
        enum ConnectionKind {ForwardLocalPort, ForwardRemotePort, SocksProxy};

        ConnectionKind kind;
        QString localHost;
        int localPort;
        QString remoteHost;
        int remotePort;
    };
    struct TunnelConfig {
        enum AuthType {Key, Password};

        QString guid;
        QString name;
        QString notes;
        AuthType authType;
        QString keyFile;
        QString username;
        QString serverAddress;
        int serverPort;
        QList<TunnelConnection> connections;

        TunnelConfig(bool valid = true);
        bool isValid() { return valid; };
        static TunnelConfig fromJson(const QString guid, const QJsonObject & json, int * error = 0);
        QJsonObject toJson() const;

    private:
        bool valid;
    };
    QList<TunnelConfig> tunnels();
    static void tunnelsConfigMigrationV2();
    // END NEW

    enum TunnelStatus {StatusUnknown, StatusRunning, StatusStopped, StatusStarting};

    // class Tunnel {
    // public:
    //     QString name;
    //     QString notes;
    //     QString guid;
    //     int kind;
    //     int authType;
    //     QString keyFile;
    //     QString username;
    //     QString serverAddress;
    //     int serverPort;
    //     QString localHost;
    //     int localPort;
    //     QString remoteHost;
    //     int remotePort;

    //     // state fields
    //     TunnelStatus status;

    //     // service methods
    //     Tunnel(const QMap<QString, QVariant> &);
    //     Tunnel();
    //     QMap<QString, QVariant> toMap() const;
    //     bool isValid() { return valid; };
    // private:
    //     bool valid;
    // };

    static TunnelsManager * instance();
    ~TunnelsManager();
    
    // QList<Tunnel> tunnels();
    TunnelStatus tunnelStatus(const QString & guid);
    TunnelConfig tunnel(const QString guid); 
    void updateTunnel(const QString & guid, const TunnelConfig & tunnel);
    QString addTunnel(const TunnelConfig & tunnel);
    void deleteTunnel(const QString & guid);
    void reload();
    void startTunnel(const QString & guid);
    void restartTunnel(const QString & guid);
    void stopTunnel(const QString & guid);
    void reorderTunnels(const QStringList & guids);

signals:
    void reloaded();

private slots:
    void processStateChanged(QProcess::ProcessState newState);
    void stderrReady();

private:
    void save();
    TunnelsManager(QObject * parent = 0);
    void logAppend(const QString & guid, const QString & text);
    struct Private;
    Private * p;
};

Q_DECLARE_METATYPE(TunnelsManager::TunnelConnection);
Q_DECLARE_METATYPE(TunnelsManager::TunnelConfig);

#endif