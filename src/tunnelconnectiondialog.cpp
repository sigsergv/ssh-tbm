#include <QtDebug>

#include "utils.h"
#include "constants.h"
#include "settings.h"
#include "tunnelconnectiondialog.h"
#include "ui_tunnelconnectiondialog.h"


struct TunnelConnectionDialog::Private
{
    Ui::TunnelConnectionDialog ui;
};

TunnelConnectionDialog::TunnelConnectionDialog(const QString & guid, QWidget *parent, Qt::WindowFlags f)
    : QDialog(parent, f)
{
    p = new Private;
    p->ui.setupUi(this);

    connect(p->ui.kindField, SIGNAL(currentIndexChanged(int)), this, SLOT(kindChanged(int)));

    // fill combo box
    p->ui.kindField->addItem(tr("SOCKS proxy"), QVariant(TunnelsManager::TunnelConnection::SocksProxy));
    p->ui.kindField->addItem(tr("Forward local port (-L)"), QVariant(TunnelsManager::TunnelConnection::ForwardLocalPort));
    p->ui.kindField->addItem(tr("Forward remote port (-R)"), QVariant(TunnelsManager::TunnelConnection::ForwardRemotePort));

    auto portValidator = new QIntValidator(1, 99999, this);
    p->ui.localPortField->setValidator(portValidator);
    p->ui.remotePortField->setValidator(portValidator);

    connect(p->ui.localHostField, SIGNAL(textChanged(const QString &)),
        this, SLOT(refreshHintsLabel(const QString &)));
    connect(p->ui.localPortField, SIGNAL(textChanged(const QString &)),
        this, SLOT(refreshHintsLabel(const QString &)));
    connect(p->ui.remoteHostField, SIGNAL(textChanged(const QString &)),
        this, SLOT(refreshHintsLabel(const QString &)));
    connect(p->ui.remotePortField, SIGNAL(textChanged(const QString &)),
        this, SLOT(refreshHintsLabel(const QString &)));
}

TunnelConnectionDialog::~TunnelConnectionDialog()
{
    delete p;
}


void TunnelConnectionDialog::kindChanged(int)
{
    auto kind = p->ui.kindField->currentData().toInt();

    switch (kind) {
    case TunnelsManager::TunnelConnection::SocksProxy:
        p->ui.remoteHostLabel->setDisabled(true);
        p->ui.remoteHostField->setDisabled(true);
        p->ui.remotePortLabel->setDisabled(true);
        p->ui.remotePortField->setDisabled(true);
        break;

    case TunnelsManager::TunnelConnection::ForwardLocalPort:
        p->ui.remoteHostLabel->setDisabled(false);
        p->ui.remoteHostField->setDisabled(false);
        p->ui.remotePortLabel->setDisabled(false);
        p->ui.remotePortField->setDisabled(false);
        break;

    case TunnelsManager::TunnelConnection::ForwardRemotePort:
        p->ui.remoteHostLabel->setDisabled(false);
        p->ui.remoteHostField->setDisabled(false);
        p->ui.remotePortLabel->setDisabled(false);
        p->ui.remotePortField->setDisabled(false);
        break;
    }    
    // arrangeItems();
    refreshHintsLabel(QString());
}


void TunnelConnectionDialog::setConnection(const TunnelsManager::TunnelConnection & tcc)
{
    switch (tcc.kind) {
    case TunnelsManager::TunnelConnection::SocksProxy:
        p->ui.kindField->setCurrentIndex(0);
        break;
    case TunnelsManager::TunnelConnection::ForwardLocalPort:
        p->ui.kindField->setCurrentIndex(1);
        break;
    case TunnelsManager::TunnelConnection::ForwardRemotePort:
        p->ui.kindField->setCurrentIndex(2);
        break;
    }
    p->ui.localHostField->setText(tcc.localHost);
    p->ui.localPortField->setText(QString::number(tcc.localPort));
    p->ui.remoteHostField->setText(tcc.remoteHost);
    p->ui.remotePortField->setText(QString::number(tcc.remotePort));
}


TunnelsManager::TunnelConnection TunnelConnectionDialog::connection() {
    TunnelsManager::TunnelConnection tcc;

    tcc.kind = static_cast<TunnelsManager::TunnelConnection::ConnectionKind>(p->ui.kindField->currentData().toInt());
    tcc.localHost = p->ui.localHostField->text().trimmed();
    tcc.localPort = p->ui.localPortField->text().toInt();
    tcc.remoteHost = p->ui.remoteHostField->text().trimmed();
    tcc.remotePort = p->ui.remotePortField->text().toInt();
    // tcc.remoteHost = p->ui.remoteHostField->text()->trimmed();
    return tcc;
}


void TunnelConnectionDialog::accept()
{
    bool validationOk = true;
    auto tcc = connection();

    if (tcc.localHost.isEmpty()) {
        validationOk = false;
        Utils::markFieldInvalid(p->ui.localHostField, tr("This field is required!"));
    } else {
        Utils::markFieldValid(p->ui.localHostField);
    }

    if (tcc.localPort < 1 || tcc.localPort > 65535) {
        validationOk = false;
        Utils::markFieldInvalid(p->ui.localPortField, tr("Invalid port value, must be a number in range between 1 and 65535!"));
    }

    switch (tcc.kind) {
    case TunnelsManager::TunnelConnection::SocksProxy:
        Utils::markFieldValid(p->ui.remoteHostField);
        Utils::markFieldValid(p->ui.remotePortField);
        Utils::markFieldValid(p->ui.localHostField);
        if (tcc.localPort < 1025 || tcc.localPort > 65535) {
            validationOk = false;
            Utils::markFieldInvalid(p->ui.localPortField, tr("Invalid port value, must be a number in range between 1025 and 65535!"));
        }
        break;

    case TunnelsManager::TunnelConnection::ForwardLocalPort:
    case TunnelsManager::TunnelConnection::ForwardRemotePort:
        // validate other fields
        if (tcc.remoteHost.isEmpty()) {
            validationOk = false;
            Utils::markFieldInvalid(p->ui.remoteHostField, tr("This field is required!"));
        } else {
            Utils::markFieldValid(p->ui.remoteHostField);
        }

        if (tcc.remotePort < 1 || tcc.remotePort > 65535) {
            validationOk = false;
            Utils::markFieldInvalid(p->ui.remotePortField, tr("Invalid port value, must be a number in range between 1 and 65535!"));
        }
        break;
    }

    if (!validationOk) {
        return;
    }

    QDialog::accept();
}


void TunnelConnectionDialog::refreshHintsLabel(const QString & _text)
{
    Q_UNUSED(_text);
    auto kind = p->ui.kindField->currentData().toInt();
    auto localHost = p->ui.localHostField->text().trimmed();
    auto localPort = p->ui.localPortField->text().toInt();
    auto remoteHost = p->ui.remoteHostField->text().trimmed();
    auto remotePort = p->ui.remotePortField->text().toInt();
    QString text;

    switch (kind) {
    case TunnelsManager::TunnelConnection::SocksProxy:
        text = tr("Accept connections to SOCKS-proxy on address <strong>%1:%2</strong>.")
            .arg(localHost)
            .arg(localPort);
        break;

    case TunnelsManager::TunnelConnection::ForwardLocalPort:
        text = tr("Forward local connections to address <strong>%1:%2</strong> through the tunnel to remote address <strong>%3:%4</strong>.")
            .arg(localHost)
            .arg(localPort)
            .arg(remoteHost)
            .arg(remotePort);
        break;

    case TunnelsManager::TunnelConnection::ForwardRemotePort:
        text = tr("Forward connections from remote network to address <strong>%3:%4</strong> through the tunnel back to address <strong>%1:%2</strong> in local network.")
            .arg(localHost)
            .arg(localPort)
            .arg(remoteHost)
            .arg(remotePort);
        break;
    }
    p->ui.hintLabel->setText(text); 
}