#ifndef TUNNELSLISTVIEW_H
#define TUNNELSLISTVIEW_H

#include <QtWidgets>

class TunnelsListView : public QListView
{
    Q_OBJECT
public:
    TunnelsListView(QWidget *parent = Q_NULLPTR);
    ~TunnelsListView();
    QString selectedTunnelGuid();

private:
    struct Private;
    Private * p;

private slots:
    void reload();
    void modelLayoutChanged(const QList<QPersistentModelIndex> &, QAbstractItemModel::LayoutChangeHint hint);
    void modelRowsRemoved(const QModelIndex &parent, int first, int last);
};

#endif