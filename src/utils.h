#ifndef UTILS_H
#define UTILS_H

#include <QtWidgets>

namespace Utils
{

void markFieldInvalid(QWidget * field, const QString & errorTooltip = QString());
void markFieldValid(QWidget * field);
QString allocateGuid();

}
#endif