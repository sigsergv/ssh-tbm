#include "settings.h"
#include "preferencesdialog.h"
#include "ui_preferencesdialog.h"

struct PreferencesDialog::Private
{
    Ui::PreferencesDialog ui;
};

PreferencesDialog::PreferencesDialog(QWidget *parent, Qt::WindowFlags f)
    : QDialog(parent, f)
{
    p = new Private;
    p->ui.setupUi(this);

    // fill uiLanguageCombo with available languages
    p->ui.uiLanguageCombo->addItem("English", "en");
    p->ui.uiLanguageCombo->addItem("Русский", "ru");

    if (Settings::getApplicationAutostart()) {
        p->ui.autostartCheckbox->setCheckState(Qt::Checked);
    }

    if (Settings::getApplicationTrayEnabled()) {
        p->ui.trayIconCheckbox->setCheckState(Qt::Checked);
    }

    if (Settings::getEventLogEnabled()) {
        p->ui.eventlogCheckbox->setCheckState(Qt::Checked);
    }

    auto lang = Settings::getTranslationLanguage();
    auto index = p->ui.uiLanguageCombo->findData(lang);
    p->ui.uiLanguageCombo->setCurrentIndex(index);

    // connect signals
    connect(p->ui.autostartCheckbox, SIGNAL(stateChanged(int)), this, SLOT(autostartCheckboxChanged(int)));
    connect(p->ui.trayIconCheckbox, SIGNAL(stateChanged(int)), this, SLOT(trayEnabledCheckboxChanged(int)));
    connect(p->ui.eventlogCheckbox, SIGNAL(stateChanged(int)), this, SLOT(eventlogEnabledChanged(int)));
    connect(p->ui.uiLanguageCombo, SIGNAL(activated(int)), this, SLOT(uiLanguageChanged(int)));
}


PreferencesDialog::~PreferencesDialog()
{
    delete p;
}


void PreferencesDialog::autostartCheckboxChanged(int state)
{
    Settings::setApplicationAutostart(state == Qt::Checked);
}


void PreferencesDialog::trayEnabledCheckboxChanged(int state)
{
    Settings::setApplicationTrayEnabled(state == Qt::Checked);
}


void PreferencesDialog::uiLanguageChanged(int index)
{
    auto lang = p->ui.uiLanguageCombo->itemData(index).toString();
    Settings::setTranslationLanguage(lang);
}


void PreferencesDialog::eventlogEnabledChanged(int state)
{
    Settings::setEventLogEnabled(state == Qt::Checked);
}

void PreferencesDialog::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::LanguageChange) {
        p->ui.retranslateUi(this);
    } else {
        QWidget::changeEvent(event);
    }
}