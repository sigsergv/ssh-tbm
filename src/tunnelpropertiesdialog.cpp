#include <QtDebug>

#include "utils.h"
#include "constants.h"
#include "settings.h"
#include "macos.h"
#include "tunnelsmanager.h"
#include "tunnelitemdelegate.h"
#include "tunnelpropertiesdialog.h"
#include "ui_tunnelpropertiesdialog.h"
#include "tunnelconnectiondialog.h"

const int CONNECTIONS_WIDGET_DATA_ROLE = Qt::UserRole + 1;

struct TunnelPropertiesDialog::Private
{
    Ui::TunnelPropertiesDialog ui;
    QString guid;
    QString error;
    QString renderConnectionDetails(const TunnelsManager::TunnelConnection & c);
    QWidget *editConnectionButton;
    QWidget *deleteConnectionButton;
};


QString TunnelPropertiesDialog::Private::renderConnectionDetails(const TunnelsManager::TunnelConnection & c)
{
    QString text;
    switch (c.kind) {
    case TunnelsManager::TunnelConnection::SocksProxy:
        text = tr("SOCKS proxy on local port %1:%2")
            .arg(c.localHost)
            .arg(c.localPort);
        break;
    case TunnelsManager::TunnelConnection::ForwardLocalPort:
        text = tr("Forward local port %1:%2 to remote port %3:%4")
            .arg(c.localHost)
            .arg(c.localPort)
            .arg(c.remoteHost)
            .arg(c.remotePort);
        break;
    case TunnelsManager::TunnelConnection::ForwardRemotePort:
        text = tr("Forward remote port %1:%2 to local port %3:%4")
            .arg(c.remoteHost)
            .arg(c.remotePort)
            .arg(c.localHost)
            .arg(c.localPort);
        break;
    }
    return text;
}

TunnelPropertiesDialog::TunnelPropertiesDialog(const QString & guid, const QString & copyFromGuid, QWidget *parent, Qt::WindowFlags f)
    : QDialog(parent, f)
{
    p = new Private;
    p->ui.setupUi(this);

    if (!guid.isEmpty()) {
        p->guid = guid;
    } else if (!copyFromGuid.isEmpty()) {
        p->guid = copyFromGuid;
    }

    connect(p->ui.authTypeField, SIGNAL(currentIndexChanged(int)), this, SLOT(authTypeChanged(int)));
    connect(p->ui.buttonBox, SIGNAL(accepted()), this, SLOT(acceptedHandler()));
    connect(p->ui.buttonBox, SIGNAL(rejected()), this, SLOT(rejectedHandler()));

    // use separate buttons sets for Macos and other OSes (make macos app look more native)
#ifdef Q_OS_OSX
    layout()->setContentsMargins(0, 15, -1, -1);
    p->ui.hlControls_1->setVisible(false);
    p->ui.addConnectionButton_2->setIcon(getMacosAddIcon());
    p->ui.editConnectionButton_2->setIcon(getMacosActionIcon());
    p->ui.deleteConnectionButton_2->setIcon(getMacosRemoveIcon());
    connect(p->ui.addConnectionButton_2, SIGNAL(clicked()), this, SLOT(addConnection()));
    connect(p->ui.editConnectionButton_2, SIGNAL(clicked()), this, SLOT(editConnection()));
    connect(p->ui.deleteConnectionButton_2, SIGNAL(clicked()), this, SLOT(deleteConnection()));
    p->editConnectionButton = p->ui.editConnectionButton_2;
    p->deleteConnectionButton = p->ui.deleteConnectionButton_2;
#else
    p->ui.hlControls_2->setVisible(false);
    connect(p->ui.addConnectionButton_1, SIGNAL(clicked()), this, SLOT(addConnection()));
    connect(p->ui.editConnectionButton_1, SIGNAL(clicked()), this, SLOT(editConnection()));
    connect(p->ui.deleteConnectionButton_1, SIGNAL(clicked()), this, SLOT(deleteConnection()));
    p->editConnectionButton = p->ui.editConnectionButton_1;
    p->deleteConnectionButton = p->ui.deleteConnectionButton_1;
#endif
    connect(p->ui.connectionsListWidget, SIGNAL(itemSelectionChanged()), this, SLOT(connectionsWidgetSelectionChanged()));

    // fill combo boxes
    p->ui.authTypeField->addItem(tr("Key-based"), QVariant(TunnelsManager::TunnelConfig::Key));
    p->ui.authTypeField->addItem(tr("Password"), QVariant(TunnelsManager::TunnelConfig::Password));

    auto portValidator = new QIntValidator(1, 99999, this);
    p->ui.serverPortField->setValidator(portValidator);

    // fill allKeysField with found and saved SSH keys
    QStringList sshKeys;
    QString keysDirectory;
#ifdef Q_OS_OSX
    keysDirectory = Settings::homePath() + ".ssh"; 
#endif

#ifdef Q_OS_LINUX
    keysDirectory = Settings::homePath() + ".ssh"; 
#endif

    if (!keysDirectory.isEmpty()) {
        QDir dir(keysDirectory);
        for (const auto fn : dir.entryList(QDir::Files)) {
            auto keyFileName = dir.filePath(fn);
            QFile file(keyFileName);
            if (!file.open(QIODevice::ReadOnly)) {
                continue;
            }
            QByteArray data = file.read(500);
            if (data.indexOf("-----BEGIN RSA PRIVATE KEY-----") != -1) {
                sshKeys << keyFileName;
                continue;
            }
            if (data.indexOf("-----BEGIN OPENSSH PRIVATE KEY-----") != -1) {
                sshKeys << keyFileName;
                continue;
            }
        }
    }

    p->ui.allKeysField->addItem(tr("Key not selected"), QVariant());
    for (const auto keyFile : sshKeys) {
        p->ui.allKeysField->addItem(keyFile, keyFile);
    }

    // initialize fields
    auto tm = TunnelsManager::instance();
    if (!p->guid.isEmpty()) {
        auto tunnel = tm->tunnel(p->guid);

        if (!copyFromGuid.isEmpty()) {
            p->ui.nameField->setText(tr("COPY: %0").arg(tunnel.name));
        } else {
            p->ui.nameField->setText(tunnel.name);
        }
        p->ui.notesField->setText(tunnel.notes);
        p->ui.serverAddressField->setText(tunnel.serverAddress);
        p->ui.serverPortField->setText(QString::number(tunnel.serverPort));

        for (const auto & tcc : tunnel.connections) {
            QListWidgetItem *newItem = new QListWidgetItem();
            newItem->setText(p->renderConnectionDetails(tcc));
            newItem->setData(CONNECTIONS_WIDGET_DATA_ROLE, QVariant::fromValue(tcc));
            p->ui.connectionsListWidget->addItem(newItem);
        }

        switch (tunnel.authType) {
        case TunnelsManager::TunnelConfig::Key:
            p->ui.authTypeField->setCurrentIndex(0);
            break;
        case TunnelsManager::TunnelConfig::Password:
            p->ui.authTypeField->setCurrentIndex(1);
            break;
        }

        // select keyfile
        auto keyFile = tunnel.keyFile;
        if (sshKeys.indexOf(keyFile) == -1) {
            p->ui.allKeysField->addItem(keyFile, keyFile);
        }
        p->ui.allKeysField->setCurrentText(keyFile);
        
        p->ui.usernameField->setText(tunnel.username);
    }

    if (!copyFromGuid.isEmpty()) {
        p->guid = QString();
    }
    
    arrangeItems();
}

TunnelPropertiesDialog::~TunnelPropertiesDialog()
{
    delete p;
}

void TunnelPropertiesDialog::authTypeChanged(int)
{
    arrangeItems();
}


void TunnelPropertiesDialog::arrangeItems()
{
    auto authType = static_cast<TunnelsManager::TunnelConfig::AuthType>(p->ui.authTypeField->currentData().toInt());
    switch (authType) {
    case TunnelsManager::TunnelConfig::Key:
        p->ui.allKeysField->setEnabled(true);
        p->ui.allKeysLabel->setEnabled(true);
        break;

    case TunnelsManager::TunnelConfig::Password:
        p->ui.allKeysField->setEnabled(false);
        p->ui.allKeysLabel->setEnabled(false);
        break;
    }
}


void TunnelPropertiesDialog::acceptedHandler()
{
    bool validationOk = true;
    TunnelsManager::TunnelConfig tunnel;
    // TunnelsManager::TunnelConnection tcc;
    QString value;

    value = p->ui.nameField->text().trimmed();
    if (value.isEmpty()) {
        validationOk = false;
        Utils::markFieldInvalid(p->ui.nameField, tr("This field is required!"));
    } else {
        Utils::markFieldValid(p->ui.nameField);
    }
    tunnel.name = value;

    value = p->ui.notesField->toPlainText();
    tunnel.notes = value;
    
    value = p->ui.serverAddressField->text().trimmed();
    if (value.isEmpty()) {
        validationOk = false;
        Utils::markFieldInvalid(p->ui.serverAddressField, tr("This field is required!"));
    } else {
        Utils::markFieldValid(p->ui.serverAddressField);
    }
    tunnel.serverAddress = value;

    value = p->ui.serverPortField->text().trimmed();
    tunnel.serverPort = value.toInt();
    if (tunnel.serverPort < 1 || tunnel.serverPort > 65535) {
        validationOk = false;
        Utils::markFieldInvalid(p->ui.serverPortField, tr("Invalid port value, must be a number in range between 1 and 65535!"));
    }

    if (!validationOk) {
        return;
    }

    // check second page
    auto authType = static_cast<TunnelsManager::TunnelConfig::AuthType>(p->ui.authTypeField->currentData().toInt());
    tunnel.authType = authType;

    value = p->ui.usernameField->text().trimmed();
    if (value.isEmpty()) {
        validationOk = false;
        Utils::markFieldInvalid(p->ui.usernameField, tr("This field is required!"));
    } else {
        Utils::markFieldValid(p->ui.usernameField);
        tunnel.username = value;
    }

    switch (authType) {
    case TunnelsManager::TunnelConfig::Key: {
        // validate fields on first stack: key-based auth
        auto itemIndex = p->ui.allKeysField->currentIndex();
        auto itemData = p->ui.allKeysField->itemData(itemIndex);
        if (!itemData.isValid()) {
            validationOk = false;
            Utils::markFieldInvalid(p->ui.allKeysField, tr("This field is required!"));
        } else {
            Utils::markFieldValid(p->ui.allKeysField);
            tunnel.keyFile = itemData.toString();
        }
        }
        break;

    case TunnelsManager::TunnelConfig::Password:
        // password based auth
        break;
    }

    if (!validationOk) {
        // switch to second tab
        p->ui.tabWidget->setCurrentIndex(1);
        return;
    }

    // p->ui.connectionsListWidget....
    tunnel.connections.clear();
    for (int i = 0; i < p->ui.connectionsListWidget->count(); ++i) {
        auto item = p->ui.connectionsListWidget->item(i);
        auto tcc = item->data(CONNECTIONS_WIDGET_DATA_ROLE).value<TunnelsManager::TunnelConnection>();
        tunnel.connections << tcc;
    }

    auto tm = TunnelsManager::instance();

    // save tunnel
    if (p->guid.isEmpty()) {
        // this is a new object, generate guid
        auto guid = tm->addTunnel(tunnel);
    } else {
        tm->updateTunnel(p->guid, tunnel);
    }

    accept();
}

void TunnelPropertiesDialog::rejectedHandler()
{
    reject();
}


void TunnelPropertiesDialog::connectionsWidgetSelectionChanged()
{
    auto selectedItems = p->ui.connectionsListWidget->selectedItems();
    if (selectedItems.size() > 0) {
        p->editConnectionButton->setEnabled(true);
        p->deleteConnectionButton->setEnabled(true);
    } else {
        p->editConnectionButton->setEnabled(false);
        p->deleteConnectionButton->setEnabled(false);
    }
}


void TunnelPropertiesDialog::addConnection()
{
    TunnelConnectionDialog d(QString(), this, Qt::Dialog | Qt::Tool);
    if (d.exec()) {
        auto tcc = d.connection();
        QListWidgetItem *newItem = new QListWidgetItem();
        newItem->setText(p->renderConnectionDetails(tcc));
        newItem->setData(CONNECTIONS_WIDGET_DATA_ROLE, QVariant::fromValue(tcc));
        p->ui.connectionsListWidget->addItem(newItem);
    }
}


void TunnelPropertiesDialog::editConnection()
{
    auto selectedItems = p->ui.connectionsListWidget->selectedItems();
    if (selectedItems.size() == 0) {
        return;
    }
    auto item = selectedItems[0];
    auto tcc = item->data(CONNECTIONS_WIDGET_DATA_ROLE).value<TunnelsManager::TunnelConnection>();
    TunnelConnectionDialog d(QString(), this);
    d.setConnection(tcc);
    if (d.exec()) {
        auto tcc = d.connection();
        item->setData(CONNECTIONS_WIDGET_DATA_ROLE, QVariant::fromValue(tcc));
        item->setText(p->renderConnectionDetails(tcc));
    }
}


void TunnelPropertiesDialog::deleteConnection()
{
    auto selectedItems = p->ui.connectionsListWidget->selectedItems();
    qDeleteAll(selectedItems);
}