#include <QtWidgets>

#include "constants.h"
#include "mainwindow.h"
#include "macos.h"
#include "settings.h"
#include "tunnelsmanager.h"
#include "tunnelslistview.h"
#include "traymanager.h"
#include "tunnelpropertiesdialog.h"
#include "preferencesdialog.h"
#include "aboutdialog.h"

struct MainWindow::Private {
    TunnelsListView * tunnels;
    QMenu * fileMenu;
    QMenu * helpMenu;
    QAction * startTunnelAction;
    QAction * restartTunnelAction;
    QAction * stopTunnelAction;
    QAction * viewTunnelLogAction;
    QAction * createTunnelAction;
    QAction * editTunnelAction;
    QAction * copyTunnelAction;
    QAction * deleteTunnelAction;
    QAction * preferencesAction;
    QAction * quitAction;
    QAction * docsAction;
    QAction * aboutAction;
};

static MainWindow * inst = 0;

static const QVariant HELP_ROLE_ABOUT(1);
static const QVariant HELP_ROLE_DOCUMENTATION(2);

MainWindow * MainWindow::instance()
{
    if (inst == 0) {
        inst = new MainWindow();
    }
    return inst;
}

MainWindow::MainWindow()
    : QMainWindow()
{
    p = new Private;

    // we use empty string, actual text will be set later in retranslateUi()
    QString emptyString;

    p->tunnels = new TunnelsListView(this);

    setWindowIcon(QIcon(":/app-blue.svg"));

    setCentralWidget(p->tunnels);
    setWindowTitle(emptyString);

    // setup toolbar
#ifdef Q_OS_OSX
    setUnifiedTitleAndToolBarOnMac(true);
#endif

    QToolBar * tb = new QToolBar(emptyString, this);
    tb->setMovable(false);
    tb->setFloatable(false);
    tb->toggleViewAction()->setVisible(false);
    tb->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    addToolBar(tb);

    // setup "File" menu
    p->fileMenu = menuBar()->addMenu(emptyString);

    p->startTunnelAction = tb->addAction(QIcon(":/start.svg"), emptyString, this, SLOT(startTunnel()));
    p->fileMenu->addAction(p->startTunnelAction);

    p->restartTunnelAction = tb->addAction(QIcon(":/start.svg"), emptyString, this, SLOT(restartTunnel()));
    p->fileMenu->addAction(p->restartTunnelAction);
    p->restartTunnelAction->setVisible(false);

    p->stopTunnelAction = tb->addAction(QIcon(":/stop.svg"), emptyString, this, SLOT(stopTunnel()));
    p->fileMenu->addAction(p->stopTunnelAction);

    p->viewTunnelLogAction = tb->addAction(QIcon(":/log.svg"), emptyString, this, SLOT(openEventLog()));
    p->fileMenu->addAction(p->viewTunnelLogAction);
    p->viewTunnelLogAction->setVisible(false);

    QWidget* spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    tb->addWidget(spacer);
    
    p->fileMenu->addSeparator();

    p->createTunnelAction = tb->addAction(QIcon(":/document-new.svg"), emptyString, this, SLOT(createTunnel()));
    p->fileMenu->addAction(p->createTunnelAction);

    p->editTunnelAction = tb->addAction(QIcon(":/document-edit.svg"), emptyString, this, SLOT(editTunnel()));
    p->fileMenu->addAction(p->editTunnelAction);

    p->copyTunnelAction = tb->addAction(QIcon(":/document-copy.svg"), emptyString, this, SLOT(copyTunnel()));
    p->fileMenu->addAction(p->copyTunnelAction);

    p->deleteTunnelAction = tb->addAction(QIcon(":/edit-delete.svg"), emptyString, this, SLOT(deleteTunnel()));
    p->fileMenu->addAction(p->deleteTunnelAction);

    p->editTunnelAction->setEnabled(false);
    p->copyTunnelAction->setEnabled(false);
    p->deleteTunnelAction->setEnabled(false);
    p->startTunnelAction->setEnabled(false);
    p->stopTunnelAction->setEnabled(false);
    p->viewTunnelLogAction->setEnabled(true);

    connect(p->tunnels->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)), 
        this, SLOT(tunnelsSelectionChanged(const QItemSelection &, const QItemSelection &)));
    connect(p->tunnels, SIGNAL(doubleClicked(const QModelIndex &)),
        this, SLOT(rowDoubleClicked(const QModelIndex &)));


#ifndef Q_OS_OSX
    p->fileMenu->addSeparator();
#endif

    QAction * action;
    // setup "Preferences" action
    action = new QAction(this);
    connect(action, SIGNAL(triggered()), this, SLOT(openPreferences()));
    action->setMenuRole(QAction::PreferencesRole);
    p->fileMenu->addAction(action);
    p->preferencesAction = action;

    // setup "Quit" action
    action = new QAction(this);
    connect(action, SIGNAL(triggered()), this, SLOT(quit()));
    action->setMenuRole(QAction::QuitRole);
    p->fileMenu->addAction(action);
#ifdef Q_OS_LINUX
    action->setShortcut(Qt::CTRL | Qt::Key_Q);
#endif
    p->quitAction = action;

    // setup "Help" menu
    p->helpMenu = menuBar()->addMenu(emptyString);
    action = p->helpMenu->addAction(emptyString, this, SLOT(helpMenuTriggered()));
    action->setData(HELP_ROLE_DOCUMENTATION);
    p->docsAction = action;

    // setup "About" action
    action = new QAction(this);
    action->setData(HELP_ROLE_ABOUT);
    connect(action, SIGNAL(triggered()), this, SLOT(helpMenuTriggered()));
    action->setMenuRole(QAction::AboutQtRole);
    p->helpMenu->addAction(action);
    p->aboutAction = action;

    // restore window geometry
    QSettings * s = Settings::settings();
    s->beginGroup("MainWindow");
    if (!restoreGeometry(s->value("geometry").toByteArray())) {
        // set default geometry
        setGeometry(100, 100, 600, 400);
    }
    s->endGroup();

    auto tm = TunnelsManager::instance();
    tm->reload();

    configChanged();
    retranslateUi();
}

MainWindow::~MainWindow()
{
    qDebug() << "main window destroyed";
}

void MainWindow::hideEvent(QHideEvent *event)
{
#ifdef Q_OS_MAC
    // hide dock icon
    setDockIconStyle(true);
#endif
    event->accept();
}

void MainWindow::showEvent(QShowEvent *event)
{
#ifdef Q_OS_MAC
    // show dock icon
    setDockIconStyle(false);
#endif
    event->accept();
}

void MainWindow::rememberGeometryAndState()
{
    QSettings * settings = Settings::settings();
    settings->beginGroup("MainWindow");
    settings->setValue("geometry", saveGeometry());
    // settings->setValue("state", saveState());
    settings->endGroup();
}

void MainWindow::moveEvent(QMoveEvent * event)
{
    rememberGeometryAndState();
    event->accept();
}

void MainWindow::resizeEvent(QResizeEvent * event)
{
    rememberGeometryAndState();
    event->accept();
}

void MainWindow::createTunnel()
{
    TunnelPropertiesDialog d(QString(), QString(), this);
    d.exec();
}

void MainWindow::editTunnel()
{
    auto guid = p->tunnels->selectedTunnelGuid();

    if (guid.isEmpty()) {
        return;
    }

    TunnelPropertiesDialog d(guid, QString(), this);
    d.exec();
}

void MainWindow::copyTunnel()
{
    auto guid = p->tunnels->selectedTunnelGuid();

    if (guid.isEmpty()) {
        return;
    }

    TunnelPropertiesDialog d(QString(), guid, this);
    d.exec();
}

void MainWindow::deleteTunnel()
{
    auto guid = p->tunnels->selectedTunnelGuid();

    if (guid.isEmpty()) {
        return;
    }

    QMessageBox confirmBox(this);
    confirmBox.setText(tr("Really delete this tunnel?"));
    confirmBox.addButton(tr("Don't delete"), QMessageBox::RejectRole);
    auto deleteButton = confirmBox.addButton(tr("Delete"), QMessageBox::AcceptRole);
    auto flags = confirmBox.windowFlags();
    confirmBox.setWindowFlags(Qt::Drawer | flags);
    confirmBox.setWindowModality(Qt::WindowModal);

    confirmBox.exec();

    if (confirmBox.clickedButton() == deleteButton) {
        auto tm = TunnelsManager::instance();
        tm->deleteTunnel(guid);
    }
}

void MainWindow::startTunnel()
{
    auto guid = p->tunnels->selectedTunnelGuid();
    
    if (guid.isEmpty()) {
        return;
    }

    auto tm = TunnelsManager::instance();
    tm->startTunnel(guid);
}

void MainWindow::restartTunnel()
{
    auto guid = p->tunnels->selectedTunnelGuid();
    
    if (guid.isEmpty()) {
        return;
    }

    auto tm = TunnelsManager::instance();
    tm->restartTunnel(guid);
}

void MainWindow::stopTunnel()
{
    auto guid = p->tunnels->selectedTunnelGuid();
    
    if (guid.isEmpty()) {
        return;
    }

    auto tm = TunnelsManager::instance();
    tm->stopTunnel(guid);
}

void MainWindow::openEventLog()
{
    auto logFilename = Settings::eventlogPath();
    QFileInfo fi(logFilename);

    if (fi.exists()) {
        QDesktopServices::openUrl(QUrl::fromLocalFile(logFilename));
    } else {
        QMessageBox box(this);
        box.setText(tr("Event log is empty!"));
        box.addButton(tr("Close"), QMessageBox::RejectRole);
        auto flags = box.windowFlags();
        box.setWindowFlags(Qt::Drawer | flags);
        box.setWindowModality(Qt::WindowModal);
        box.exec();
    }
}

void MainWindow::quit()
{
    QCoreApplication::quit();
}


void MainWindow::tunnelsSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    bool notEmptySelection = !selected.isEmpty();

    auto tm = TunnelsManager::instance();

    if (notEmptySelection) {
        for (const auto & index : selected.constFirst().indexes()) {
            auto tunnelGuid = index.data().toString();
            const auto data = tm->tunnel(tunnelGuid);
            auto status = tm->tunnelStatus(tunnelGuid);
            switch (status) {
            case TunnelsManager::StatusRunning:
                p->startTunnelAction->setEnabled(false);
                p->startTunnelAction->setVisible(false);
                p->restartTunnelAction->setVisible(true);
                p->stopTunnelAction->setEnabled(true);
                break;
            case TunnelsManager::StatusUnknown:
            case TunnelsManager::StatusStopped:
                p->startTunnelAction->setEnabled(true);
                p->startTunnelAction->setVisible(true);
                p->restartTunnelAction->setVisible(false);
                p->stopTunnelAction->setEnabled(false);
                break;
            case TunnelsManager::StatusStarting:
                p->startTunnelAction->setEnabled(false);
                p->restartTunnelAction->setVisible(false);
                p->stopTunnelAction->setEnabled(true);
            }
            break;
        }
    } else {
        p->startTunnelAction->setEnabled(false);
        p->restartTunnelAction->setVisible(false);
        p->stopTunnelAction->setEnabled(false);
    }

    p->editTunnelAction->setEnabled(notEmptySelection);
    p->copyTunnelAction->setEnabled(notEmptySelection);
    p->deleteTunnelAction->setEnabled(notEmptySelection);
}

void MainWindow::helpMenuTriggered()
{
    QAction * sender = qobject_cast<QAction*>(this->sender());

    if (sender->data() == HELP_ROLE_ABOUT) {
        AboutDialog d;
        d.exec();
    } else if (sender->data() == HELP_ROLE_DOCUMENTATION) {
        QString url = "https://gitlab.com/sigsergv/ssh-tbm/wikis/How-to-use-SSH-TBM-(version-2.x)";

        if (Settings::getTranslationLanguage() == "ru") {
            url = "https://gitlab.com/sigsergv/ssh-tbm/wikis/Как-пользоваться-SSH-TBM-(версия-2.x)";
        }

        QDesktopServices::openUrl(url);
    }
}


void MainWindow::openPreferences()
{
    PreferencesDialog d;
    d.exec();
}

void MainWindow::rowDoubleClicked(const QModelIndex &index)
{
    // if tunnel is stopped then start it
    // otherwise - stop

    auto guid = p->tunnels->selectedTunnelGuid();
    
    if (guid.isEmpty()) {
        return;
    }

    auto tm = TunnelsManager::instance();
    auto status = tm->tunnelStatus(guid);

    qDebug() << "status" << status;
    if (status == TunnelsManager::StatusStopped || status == TunnelsManager::StatusUnknown) {
        tm->startTunnel(guid);
    } else {
        tm->stopTunnel(guid);
    }
}


void MainWindow::retranslateUi()
{
    auto windowTitle = tr("SSH Tunnels");

#ifdef QT_DEBUG
    windowTitle += " [DEBUG VERSION]";
#endif

    setWindowTitle(windowTitle);
    p->fileMenu->setTitle(tr("&File"));
    p->helpMenu->setTitle(tr("&Help"));
    p->startTunnelAction->setText(tr("Start"));
    p->restartTunnelAction->setText(tr("Restart"));
    p->stopTunnelAction->setText(tr("Stop"));
    p->viewTunnelLogAction->setText(tr("Event log"));
    p->createTunnelAction->setText(tr("Create tunnel"));
    p->editTunnelAction->setText(tr("Edit tunnel"));
    p->copyTunnelAction->setText(tr("Copy"));
    p->deleteTunnelAction->setText(tr("Delete tunnel"));
    p->preferencesAction->setText(tr("Preferences"));
    p->quitAction->setText(tr("Quit ssh-tbm"));
    p->docsAction->setText(tr("Documentation"));
    p->aboutAction->setText(AboutDialog::tr("About ssh-tbm"));
    TrayManager::instance()->retranslateUi();

}

void MainWindow::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::LanguageChange) {
        retranslateUi();
    } else {
        QWidget::changeEvent(event);
    }
}

void MainWindow::configChanged()
{
    if (Settings::getEventLogEnabled()) {
        p->viewTunnelLogAction->setVisible(true);
    } else {
        p->viewTunnelLogAction->setVisible(false);
    }
}


void MainWindow::forceShow()
{
    show();
    raise();
    activateWindow();
}

void MainWindow::showErrorMessageBox(const QString & msg)
{
    forceShow();

    QMessageBox box(this);
    box.setText(msg);
    box.addButton(tr("Close"), QMessageBox::RejectRole);
    auto flags = box.windowFlags();
    box.setIcon(QMessageBox::Critical);
    box.setWindowFlags(Qt::Drawer | flags);
    box.setWindowModality(Qt::WindowModal);
    box.exec();
}