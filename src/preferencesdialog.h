#ifndef PREFERENCESDIALOG_H
#define PREFERENCESDIALOG_H

#include <QtWidgets>

class PreferencesDialog : public QDialog
{
    Q_OBJECT
public:
    PreferencesDialog(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
    ~PreferencesDialog();

    void changeEvent(QEvent *event);

private slots:
    void autostartCheckboxChanged(int state);
    void trayEnabledCheckboxChanged(int state);
    void uiLanguageChanged(int index);
    void eventlogEnabledChanged(int state);

private:
    struct Private;
    Private * p;
};

#endif