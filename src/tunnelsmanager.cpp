#include <QtDebug>

#include "utils.h"
#include "settings.h"
#include "tunnelsmanager.h"
#include "mainwindow.h"

#ifdef QT_DEBUG
static const QString TUNNELS_JSON_FILE(Settings::settingsPath() + "/tunnels-debug.json");
#else
static const QString TUNNELS_JSON_FILE(Settings::settingsPath() + "/tunnels.json");
#endif

enum JsonParsingErrors {NoError, ConnectionsBlockInvalid, StringValueRequired, IntValueRequired, SomeOtherError};

struct TunnelsManager::Private
{
    QMap<QString, TunnelsManager::TunnelConfig> tunnelsCache;
    QList<QString> tunnelsCacheOrder;
    QMap<QString, QProcess*> processes;
    QMap<QString, TunnelsManager::TunnelStatus> statuses;
    QSet<QProcess*> restartTriggers;
};

static TunnelsManager * inst = 0;

TunnelsManager::TunnelConfig::TunnelConfig(bool _valid)
{
    valid = _valid;
}


/**
 * Reads JSON and creates ordered list of TunnelConfig records.
 */
QList<TunnelsManager::TunnelConfig> TunnelsManager::tunnels()
{
    // read tunnels list from a JSON
    QList<TunnelsManager::TunnelConfig> tunnels;
    QFile jf(TUNNELS_JSON_FILE);
    if (!jf.exists()) {
        return tunnels;
    }
    jf.open(QIODevice::ReadOnly | QIODevice::Text);
    auto rawJsonData = jf.read(1024*1024);  // one megabyte ought to be enough for anybody
    jf.close();
    QJsonParseError jsonParseError;
    auto jd = QJsonDocument::fromJson(rawJsonData, &jsonParseError);
    if (jsonParseError.error != QJsonParseError::NoError) {
        qDebug() << "Invalid tunnels JSON document";
        return tunnels;
    }
    auto jo = jd.object();
    auto tunnelsOrderJson = jo["order"];
    auto tunnelsJson = jo["tunnels"].toObject();
    if (tunnelsJson.empty()) {
        qDebug() << "Invalid or missing tunnels object";
        return tunnels;
    }
    QList<QString> tunnelGuids;
    if (tunnelsOrderJson.isUndefined() || !tunnelsOrderJson.isArray() || tunnelsOrderJson.toArray().empty()) {
        qDebug() << "Tunnels order is not defined, use all defined tunnels.";
        for (auto i = tunnelsJson.constBegin(); i != tunnelsJson.constEnd(); ++i) {
            tunnelGuids << i.key();
        }
    } else {
        for (auto i : tunnelsOrderJson.toArray()) {
            if (!i.isString()) {
                continue;
            }
            tunnelGuids << i.toString();
        }
    }
    p->tunnelsCache.clear();
    p->tunnelsCacheOrder.clear();
    for (auto guid : tunnelGuids) {
        auto tj = tunnelsJson[guid];
        if (tj.isUndefined() || !tj.isObject()) {
            // qDebug() << "Invalid tunnel JSON" << tj;
            continue;
        }
        int error = NoError;
        auto t = TunnelConfig::fromJson(guid, tj.toObject(), &error);
        if (error == NoError) {
            tunnels << t;
            p->tunnelsCache[guid] = t;
            p->tunnelsCacheOrder << guid;
        } else {
            qDebug() << "error" << error;
        }
    }
    return tunnels;
}

/**
 * Saves internal cache of tunnel objects back to JSON config file.
 */
void TunnelsManager::save()
{
    QJsonArray orderJson;
    for (const auto & g : p->tunnelsCacheOrder) {
        orderJson << g;
    }
    QJsonObject tunnelsJson;
    for (const auto & t : p->tunnelsCache) {
        tunnelsJson[t.guid] = t.toJson();
    }
    QJsonObject json;
    json["order"] = orderJson;
    json["tunnels"] = tunnelsJson;
    QJsonDocument doc(json);
    auto jsonBytes = doc.toJson(QJsonDocument::Indented);

    QFile jf(TUNNELS_JSON_FILE);
    // QFile jf(TUNNELS_JSON_FILE+".n");
    jf.open(QIODevice::WriteOnly | QIODevice::Text);
    jf.write(jsonBytes);
    jf.close();
}

TunnelsManager::TunnelConfig TunnelsManager::TunnelConfig::fromJson(const QString guid, const QJsonObject & json, int * error)
{
    int _error;
    if (!error) {
        error = &_error;
    }
    QJsonValue v;
    QString s;
    int n;
    TunnelsManager::TunnelConfig t;
    t.guid = guid;

    v = json["name"];
    if (!v.isString()) {
        *error = StringValueRequired;
        return t;
    }
    t.name = v.toString();

    v = json["notes"];
    if (!v.isString()) {
        *error = StringValueRequired;
        return t;
    }
    t.notes = v.toString();

    v = json["authType"];
    if (!v.isString()) {
        *error = StringValueRequired;
        return t;
    }
    s = v.toString();
    if (s == "key") {
        t.authType = TunnelsManager::TunnelConfig::Key;
    } else if (s == "password") {
        t.authType = TunnelsManager::TunnelConfig::Password;
    } else {
        *error = SomeOtherError;
        return t;
    }

    v = json["username"];
    if (!v.isString()) {
        *error = StringValueRequired;
        return t;
    }
    t.username = v.toString();

    v = json["serverAddress"];
    if (!v.isString()) {
        *error = StringValueRequired;
        return t;
    }
    t.serverAddress = v.toString();

    v = json["serverPort"];
    n = v.toInt(-1);
    if (n == -1) {
        *error = IntValueRequired;
        return t;
    }
    t.serverPort = n;

    v = json["keyFile"];
    if (!v.isString()) {
        *error = StringValueRequired;
        return t;
    }
    t.keyFile = v.toString();

    auto cj = json["connections"].toArray();
    // // allow empty connections, just ignore them
    // if (cj.empty()) {
    //     *error = ConnectionsBlockInvalid;
    //     return t;
    // }
    for (const auto & x : cj) {
        if (!x.isObject()) {
            continue;
        }
        auto y = x.toObject();
        TunnelsManager::TunnelConnection tc;
        v = y["kind"];
        if (!v.isString()) {
            continue;
        }
        s = v.toString();
        if (s == "ForwardLocalPort") {
            tc.kind = TunnelsManager::TunnelConnection::ForwardLocalPort;
        } else if (s == "ForwardRemotePort") {
            tc.kind = TunnelsManager::TunnelConnection::ForwardRemotePort;
        } else if (s == "SocksProxy") {
            tc.kind = TunnelsManager::TunnelConnection::SocksProxy;
        } else {
            continue;
        }

        v = y["localHost"];
        if (!v.isString()) {
            continue;
        }
        tc.localHost = v.toString();

        v = y["localPort"];
        n = v.toInt(-1);
        if (n == -1) {
            continue;
        }
        tc.localPort = n;

        v = y["remoteHost"];
        if (!v.isString()) {
            continue;
        }
        tc.remoteHost = v.toString();

        v = y["remotePort"];
        n = v.toInt(-1);
        if (n == -1) {
            continue;
        }
        tc.remotePort = n;

        t.connections << tc;
    }

    *error = 0;
    return t;
}

QJsonObject TunnelsManager::TunnelConfig::toJson() const
{
    QJsonObject json;
    json["name"] = name;
    json["notes"] = notes;
    json["username"] = username;
    json["keyFile"] = keyFile;
    json["serverAddress"] = serverAddress;
    json["serverPort"] = serverPort;
    if (authType == TunnelsManager::TunnelConfig::Key) {
        json["authType"] = "key";
    } else if (authType == TunnelsManager::TunnelConfig::Password) {
        json["authType"] = "password";
    }
    QJsonArray connectionsJson;
    for (const auto & tcc : connections) {
        QJsonObject c;
        c["localHost"] = tcc.localHost;
        c["localPort"] = tcc.localPort;
        c["remoteHost"] = tcc.remoteHost;
        c["remotePort"] = tcc.remotePort;
        switch (tcc.kind) {
        case TunnelsManager::TunnelConnection::SocksProxy:
            c["kind"] = "SocksProxy";
            break;
        case TunnelsManager::TunnelConnection::ForwardLocalPort:
            c["kind"] = "ForwardLocalPort";
            break;
        case TunnelsManager::TunnelConnection::ForwardRemotePort:
            c["kind"] = "ForwardRemotePort";
            break;
        }
        connectionsJson << c;
    }
    json["connections"] = connectionsJson;
    return json;
}

void TunnelsManager::tunnelsConfigMigrationV2()
{
    // read tunnels data from old settings and convert to new JSON format
    QSettings * s = Settings::settings();
    s->beginGroup("tunnels");
    auto migration = s->value("migration").toString();
    if (migration == "complete") {
        return;
    }
    s->endGroup(); // "tunnels"

    QJsonArray orderJson;
    QJsonObject tunnelsJson;
    for (const auto & t : Settings::tunnels()) {
        TunnelsManager::TunnelConfig tc;
        tc.guid = t["guid"].toString();
        tc.name = t["name"].toString();
        tc.notes = t["notes"].toString();
        tc.username = t["username"].toString();
        tc.keyFile = t["keyfile"].toString();
        tc.serverAddress = t["serveraddress"].toString();
        tc.serverPort = t["serverport"].toInt();
        auto authType = t["authtype"].toInt();
        if (authType == 1) {
            tc.authType = TunnelsManager::TunnelConfig::Key;
        } else if (authType == 2) {
            tc.authType = TunnelsManager::TunnelConfig::Password;
        }
        auto kind = t["kind"].toInt();
        TunnelsManager::TunnelConnection tcc;
        if (kind == 1) {
            tcc.kind = TunnelsManager::TunnelConnection::SocksProxy;
        } else if (kind == 2) {
            tcc.kind = TunnelsManager::TunnelConnection::ForwardLocalPort;
        } else if (kind == 3) {
            tcc.kind = TunnelsManager::TunnelConnection::ForwardRemotePort;
        }
        tcc.localHost = t["localhost"].toString();
        tcc.localPort = t["localport"].toInt();
        tcc.remoteHost = t["remotehost"].toString();
        tcc.remotePort = t["remoteport"].toInt();
        tc.connections << tcc;
        orderJson << tc.guid;
        tunnelsJson[tc.guid] = tc.toJson();
    }
    QJsonObject json;
    json["order"] = orderJson;
    json["tunnels"] = tunnelsJson;
    QJsonDocument doc(json);
    auto jsonBytes = doc.toJson(QJsonDocument::Indented);
    QFile jf(TUNNELS_JSON_FILE);
    jf.open(QIODevice::WriteOnly | QIODevice::Text);
    jf.write(jsonBytes);
    jf.close();

    s->beginGroup("tunnels");
    s->setValue("migration", "complete");
    s->endGroup(); // "tunnels"
}


TunnelsManager * TunnelsManager::instance()
{
    if (inst == 0) {
        inst = new TunnelsManager();
    }

    return inst;
}

TunnelsManager::TunnelsManager(QObject * parent)
    : QObject(parent)
{
    p = new Private;
}


void TunnelsManager::reload()
{
    emit reloaded();
}

TunnelsManager::~TunnelsManager()
{
    // destroy all running tunnels
    for (const auto & x : p->processes) {
        stopTunnel(x->property("guid").toString());
    }
    delete p;
}

TunnelsManager::TunnelStatus TunnelsManager::tunnelStatus(const QString & guid)
{
    if (!p->statuses.contains(guid)) {
        return TunnelsManager::StatusUnknown;
    }

    return p->statuses[guid];
}


TunnelsManager::TunnelConfig TunnelsManager::tunnel(const QString guid)
{
    if (!p->tunnelsCache.contains(guid)) {
        TunnelsManager::TunnelConfig t(false);
        return t;
    }
    return p->tunnelsCache[guid];
}


void TunnelsManager::updateTunnel(const QString & guid, const TunnelsManager::TunnelConfig & _tunnel)
{
    TunnelsManager::TunnelConfig tunnel = _tunnel;
    tunnel.guid = guid;
    p->tunnelsCache[guid] = tunnel;
    save();
    tunnels();  // re-read JSON
    emit reloaded();
}


QString TunnelsManager::addTunnel(const TunnelsManager::TunnelConfig & _tunnel)
{
    TunnelsManager::TunnelConfig tunnel = _tunnel;
    tunnel.guid = Utils::allocateGuid();
    p->tunnelsCache[tunnel.guid] = tunnel;
    p->tunnelsCacheOrder.append(tunnel.guid);
    save();
    tunnels();  // re-read JSON
    emit reloaded();
    return tunnel.guid;
}


void TunnelsManager::deleteTunnel(const QString & guid)
{
    p->tunnelsCacheOrder.removeAll(guid);
    p->tunnelsCache.remove(guid);
    save();
    tunnels();  // re-read JSON
    emit reloaded();
}

void TunnelsManager::startTunnel(const QString & guid)
{
    if (!p->tunnelsCache.contains(guid)) {
        return;
    }
    auto tunnel = p->tunnelsCache[guid];

    auto mw = MainWindow::instance();
    QString program = Settings::binaryPath(Settings::SshBin);
    QStringList arguments;

    // start with enabled debug output to control connection progress
    arguments << "-v";
    // arguments << "-vv";

    if (tunnel.connections.length() == 0) {
        auto msg = MainWindow::tr("Cannot start: no defined connections in this entry.");
        mw->showErrorMessageBox(msg);
        return;
    }

    for (const auto c : tunnel.connections) {
        switch (c.kind) {
        case TunnelsManager::TunnelConnection::SocksProxy:
            arguments << "-D" << QString("%1:%2")
                .arg(c.localHost)
                .arg(c.localPort);
            break;

        case TunnelsManager::TunnelConnection::ForwardLocalPort:
            // bind_address:port:host:hostport
            arguments << "-L" << QString("%1:%2:%3:%4")
                .arg(c.localHost)
                .arg(c.localPort)
                .arg(c.remoteHost)
                .arg(c.remotePort);
            break;

        case TunnelsManager::TunnelConnection::ForwardRemotePort:
            // bind_address:port:host:hostport
            arguments << "-R" << QString("%1:%2:%3:%4")
                .arg(c.remoteHost)
                .arg(c.remotePort)
                .arg(c.localHost)
                .arg(c.localPort)
                ;
            break;

        default:
            // not supported kind
            return;
        }
    }

    arguments << "-l" << tunnel.username;
    arguments << "-T"; // Disable pseudo-terminal allocation.
    arguments << "-x"; // Disable X11 forwarding.
    arguments << "-N"; // Do not execute commands, we need just port forwarding
    arguments << "-p" << QString::number(tunnel.serverPort);

    // // Windows is not compatible with -M option
    // arguments << "-M"; // start SSH in master mode
    // arguments << "-S" << Settings::controlSocketPath() + "/" + guid;  // path to control socket

    switch (tunnel.authType) {
    case TunnelsManager::TunnelConfig::Key:
        arguments << "-o" << "PreferredAuthentications=publickey";
        arguments << "-i" << tunnel.keyFile;
        break;

    case TunnelsManager::TunnelConfig::Password:
        arguments << "-o" << "PreferredAuthentications=password";
        break;

    default:
        // not supported auth kind
        return;
    }

    arguments << tunnel.serverAddress;

    QProcess * process = new QProcess(this);
    auto env = QProcessEnvironment::systemEnvironment();

    // set SSH_ASKPASS environment variable, it's required for host key confirmation and password request
    env.insert("SSH_ASKPASS", Settings::binaryPath(Settings::SshAskPassBin));
    
#ifdef Q_OS_WIN32    
    env.insert("DISPLAY", ":0.0");  // workaround for windows
#endif

    // disassociate program from tty, it's required for SSH_ASKPASS to work
    env.remove("TTY");

    process->setProcessEnvironment(env);    

    process->setProperty("guid", guid);
    process->setProperty("localHost", tunnel.connections[0].localHost);
    process->setProperty("localPort", tunnel.connections[0].localPort);
    process->setProperty("kind", tunnel.connections[0].kind);
    connect(process, SIGNAL(stateChanged(QProcess::ProcessState)), this, SLOT(processStateChanged(QProcess::ProcessState)));
    connect(process, SIGNAL(readyReadStandardError()), this, SLOT(stderrReady()));

    process->setStandardInputFile(QProcess::nullDevice());

    qDebug() << "Start tunnel using command:";
    qDebug() << program << arguments.join(" ");

    process->start(program, arguments);

    logAppend(guid, qgetenv("SSH_AUTH_SOCK")),

    logAppend(guid, MainWindow::tr("Starting tunnel \"%1\"").arg(tunnel.name));

    p->processes[guid] = process;
    p->statuses[guid] = StatusStarting;
}

void TunnelsManager::restartTunnel(const QString & guid)
{
    if (!p->tunnelsCache.contains(guid)) {
        return;
    }
    auto tunnel = p->tunnelsCache[guid];

    if (!p->processes.contains(tunnel.guid)) {
        return;
    }

    auto process = p->processes[guid];
    logAppend(guid, MainWindow::tr("Tunnel terminated by user (restart)"));
    p->restartTriggers.insert(process);
    process->kill();
}

void TunnelsManager::stopTunnel(const QString & guid)
{
    if (!p->tunnelsCache.contains(guid)) {
        return;
    }
    auto tunnel = p->tunnelsCache[guid];

    if (!p->processes.contains(tunnel.guid)) {
        return;
    }

    auto process = p->processes[guid];
    logAppend(guid, MainWindow::tr("Tunnel terminated by user"));
    process->kill();
}


void TunnelsManager::processStateChanged(QProcess::ProcessState s)
{
    QProcess * process = qobject_cast<QProcess*>(sender());
    if (process == 0) {
        return;
    }
    auto guid = process->property("guid").toString();

    if (s == QProcess::NotRunning) {
        // destroy process object
        process->deleteLater();
        p->processes.remove(guid);
        p->statuses.remove(guid);
        logAppend(guid, MainWindow::tr("Connection closed"));

        if (p->restartTriggers.contains(process)) {
            startTunnel(guid);
        }
    }
    p->restartTriggers.remove(process);
    emit reloaded();
}

void TunnelsManager::stderrReady()
{
    QProcess * process = qobject_cast<QProcess*>(sender());
    if (process == 0) {
        return;
    }
    auto guid = process->property("guid").toString();
    auto data = process->readAllStandardError();
    QList<QByteArray> debug1Lines;
    QList<QByteArray> debug2Lines;
    QList<QByteArray> otherLines;

    QByteArray lsp("\r");
    QByteArray debug1("debug1: ");
    auto debug1Len = debug1.length();
    QByteArray debug2("debug2: ");
    auto debug2Len = debug1.length();
    QByteArray empty;

    for (const auto & x : data.split('\n')) {
        auto line = x.simplified();
        if (line.length() == 0) {
            continue;
        }
        if (line.startsWith(debug1)) {
            debug1Lines << line.mid(debug1Len);
        } else if (line.startsWith(debug2)) {
            debug2Lines << line.mid(debug2Len);
        } else {
            otherLines << line;
        }
    }

#ifdef QT_DEBUG
    for (const QString & c : debug1Lines) {
        qDebug() << "[D1] " << c;
    }
    for (const QString & c : debug2Lines) {
        qDebug() << "[D2] " << c;
    }
    for (const QString & c : otherLines) {
        qDebug() << "[--] " << c;
    }
#endif

    // look for failures first

    auto mw = MainWindow::instance();
    auto kind = process->property("kind").toInt();

    bool isFailure = false;
    for (const auto & line : otherLines) {
        if (kind == TunnelsManager::TunnelConnection::SocksProxy &&
            line.startsWith("bind: Address already in use")) 
        {
            // TESTING.md, case:TUN-SOCKS-001
            auto msg = MainWindow::tr("Address \"%1:%2\" already in use")
                .arg(process->property("localHost").toString())
                .arg(process->property("localPort").toString());
            logAppend(guid, msg);
            stopTunnel(guid);
            mw->showErrorMessageBox(msg);
            isFailure = true;
            break;
        }
        if (kind == TunnelsManager::TunnelConnection::ForwardLocalPort &&
            line.startsWith("listen: Address already in use")) 
        {
            // TESTING.md, case:TUN-FWD-LOC-001
            auto msg = MainWindow::tr("Address \"%1:%2\" already in use")
                .arg(process->property("localHost").toString())
                .arg(process->property("localPort").toString());
            logAppend(guid, msg);
            stopTunnel(guid);
            mw->showErrorMessageBox(msg);
            isFailure = true;
            break;
        }
        if ((kind == TunnelsManager::TunnelConnection::SocksProxy || kind == TunnelsManager::TunnelConnection::ForwardLocalPort) &&
            line.startsWith("bind: Cannot assign requested address"))
        {
            // TESTING.md, case:TUN-SOCKS-002, case:TUN-FWD-LOC-002
            auto msg = MainWindow::tr("Cannot assign address, \"%1\" not found.")
                .arg(process->property("localHost").toString());
            logAppend(guid, msg);
            stopTunnel(guid);
            mw->showErrorMessageBox(msg);
            isFailure = true;
            break;
        }
        if ((kind == TunnelsManager::TunnelConnection::SocksProxy || kind == TunnelsManager::TunnelConnection::ForwardLocalPort) &&
            line.startsWith("channel_setup_fwd_listener_tcpip: getaddrinfo") && 
            (line.endsWith(": Name or service not known") ||
                line.endsWith(": nodename nor servname provided, or not known"))

            )
        {
            // TESTING.md, case:TUN-SOCKS-003
            auto msg = MainWindow::tr("Cannot resolve address \"%1\".")
                .arg(process->property("localHost").toString());
            logAppend(guid, msg);
            stopTunnel(guid);
            mw->showErrorMessageBox(msg);
            isFailure = true;
            break;
        }
        if (line.startsWith("Permission denied")) {
            auto msg = MainWindow::tr("Connection failed with error: Permission denied");
            logAppend(guid, msg);
            break;
        }
    }
    
    if (isFailure) {
        return;
    }

    // somewhere
    switch (kind) {
    case TunnelsManager::TunnelConnection::SocksProxy: {
        // TESTING.md, case:TUN-SOCKS-SUCCESS
        QByteArray successTrigger1("channel 0: new [port listener]");
        QByteArray successTrigger2("channel 0: new port-listener [port listener]");
        for (const auto & line : debug1Lines) {
            if (line.startsWith(successTrigger1) || line.startsWith(successTrigger2)) {
                p->statuses[guid] = StatusRunning;
                emit reloaded();
                logAppend(guid, MainWindow::tr("Connection established"));
                break;
            }
        }
        } break;
    case TunnelsManager::TunnelConnection::ForwardLocalPort: {
        // TESTING.md, case:TUN-FWD-LOC-SUCCESS
        QByteArray successTrigger1("channel 0: new [port listener]");
        QByteArray successTrigger2("channel 0: new port-listener [port listener]");
        for (const auto & line : debug1Lines) {
            if (line.startsWith(successTrigger1) || line.startsWith(successTrigger2)) {
                p->statuses[guid] = StatusRunning;
                emit reloaded();
                logAppend(guid, MainWindow::tr("Connection established"));
                break;
            }
        }
        } break;

    case TunnelsManager::TunnelConnection::ForwardRemotePort: {
        // TESTING.md, case:TUN-FWD-REMOTE-SUCCESS
        QByteArray successTrigger("remote forward success for: listen");
        for (const auto & line : debug1Lines) {
            if (line.startsWith(successTrigger)) {
                p->statuses[guid] = StatusRunning;
                emit reloaded();
                logAppend(guid, MainWindow::tr("Connection established"));
                break;
            }
        }
        } break;
    }

}

void TunnelsManager::logAppend(const QString & guid, const QString & text)
{
    if (!Settings::getEventLogEnabled()) {
        return;
    }

    auto logFilename = Settings::eventlogPath();
    QFile log(logFilename);

    if (!log.open(QIODevice::Append | QIODevice::Text)) {
        return;
    }

    auto now = QDateTime::currentDateTime();
    auto ts = QString("[%1] [%2] %3\n")
        .arg(now.toString(Qt::ISODate))
        .arg(guid)
        .arg(text);

    log.write(ts.toUtf8());
    log.flush();
}


void TunnelsManager::reorderTunnels(const QStringList & guids)
{
    p->tunnelsCacheOrder = guids;
    save();
    emit reloaded();
}