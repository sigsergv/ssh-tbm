#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

#include <QtWidgets>

class AboutDialog : public QDialog
{
    Q_OBJECT
public:
    AboutDialog(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
    ~AboutDialog();

private:
    struct Private;
    Private * p;
};

#endif