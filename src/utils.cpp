#include "utils.h"

namespace Utils
{

void markFieldInvalid(QWidget * field, const QString & errorTooltip)
{
    field->setStyleSheet("background-color: #FFE1E1;");
    if (!errorTooltip.isEmpty()) {
        field->setToolTip(errorTooltip);
    }
}

void markFieldValid(QWidget * field)
{
    field->setStyleSheet("");
    field->setToolTip(QString());
}

QString allocateGuid()
{
    return QUuid::createUuid().toString().mid(1, 36).toUpper();
}

}