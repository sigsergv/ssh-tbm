<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="aboutdialog.ui" line="20"/>
        <location filename="mainwindow.cpp" line="443"/>
        <source>About ssh-tbm</source>
        <translation>О программе ssh-tbm</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="32"/>
        <source>SSH Tunnel Boring Machine</source>
        <translation></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="57"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="86"/>
        <source>Copyright © 2017-2022 Sergey Stolyarov</source>
        <oldsource>Copyright © 2017-2021 Sergey Stolyarov</oldsource>
        <translation>Copyright © 2017-2022 Sergey Stolyarov</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="102"/>
        <source>SSH-TBM is free Software licensed as GPL v3</source>
        <translation>SSH-TBM — это свободное программное обеспечение, распространяемое по лицензии GPL v3 </translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="115"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://gitlab.com/sigsergv/ssh-tbm/&quot;&gt;https://gitlab.com/sigsergv/ssh-tbm/&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="141"/>
        <source>Some icons are made by:</source>
        <translation>Отдельные иконки сделаны:</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="151"/>
        <source>&lt;a href=&quot;https://www.freepik.com/&quot;&gt;www.freepik.com/&lt;/a&gt;&lt;br&gt;
&lt;a href=&quot;https://www.flaticon.com/authors/pause08&quot;&gt;www.flaticon.com/authors/pause08&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="199"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="423"/>
        <source>SSH Tunnels</source>
        <translation>SSH туннели</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="432"/>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="433"/>
        <source>Restart</source>
        <translation>Рестарт</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Stop</source>
        <translation>Стоп</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="435"/>
        <source>Event log</source>
        <translation>Журнал</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="436"/>
        <source>Create tunnel</source>
        <translation>Создать туннель</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="437"/>
        <source>Edit tunnel</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="438"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="439"/>
        <source>Delete tunnel</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="440"/>
        <source>Preferences</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="441"/>
        <location filename="traymanager.cpp" line="49"/>
        <location filename="traymanager.cpp" line="201"/>
        <source>Quit ssh-tbm</source>
        <translation>Выход из ssh-tbm</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="431"/>
        <source>&amp;Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="442"/>
        <source>Documentation</source>
        <translation>Документация</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="255"/>
        <source>Really delete this tunnel?</source>
        <translation>Действительно удалить этот туннель?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="256"/>
        <source>Don&apos;t delete</source>
        <translation>Не удалять</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="257"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="315"/>
        <source>Event log is empty!</source>
        <translation>Журнал пустой!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="316"/>
        <location filename="mainwindow.cpp" line="480"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="430"/>
        <source>&amp;File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="tunnelsmanager.cpp" line="449"/>
        <source>Cannot start: no defined connections in this entry.</source>
        <translation>Нельзя запустить: не задано ни одного соединения.</translation>
    </message>
    <message>
        <location filename="tunnelsmanager.cpp" line="545"/>
        <source>Starting tunnel &quot;%1&quot;</source>
        <translation>Запускаем туннель &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="tunnelsmanager.cpp" line="563"/>
        <source>Tunnel terminated by user (restart)</source>
        <translation>Туннель остановлен пользователем (рестарт)</translation>
    </message>
    <message>
        <location filename="tunnelsmanager.cpp" line="580"/>
        <source>Tunnel terminated by user</source>
        <translation>Туннель закрыт пользователем</translation>
    </message>
    <message>
        <location filename="tunnelsmanager.cpp" line="598"/>
        <source>Connection closed</source>
        <translation>Соединение закрыто</translation>
    </message>
    <message>
        <location filename="tunnelsmanager.cpp" line="664"/>
        <location filename="tunnelsmanager.cpp" line="677"/>
        <source>Address &quot;%1:%2&quot; already in use</source>
        <translation>Адрес &quot;%1:%2&quot; уже используется</translation>
    </message>
    <message>
        <location filename="tunnelsmanager.cpp" line="690"/>
        <source>Cannot assign address, &quot;%1&quot; not found.</source>
        <translation>Не могу присвоить адрес, &quot;%1&quot; не найден.</translation>
    </message>
    <message>
        <location filename="tunnelsmanager.cpp" line="706"/>
        <source>Cannot resolve address &quot;%1&quot;.</source>
        <translation>Не могу преобразовать адрес &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="tunnelsmanager.cpp" line="715"/>
        <source>Connection failed with error: Permission denied</source>
        <translation>Соединение прервалось с ошибкой: Permission denied </translation>
    </message>
    <message>
        <location filename="tunnelsmanager.cpp" line="734"/>
        <location filename="tunnelsmanager.cpp" line="746"/>
        <location filename="tunnelsmanager.cpp" line="759"/>
        <source>Connection established</source>
        <translation>Соединение установлено</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="preferencesdialog.ui" line="14"/>
        <source>SSH TBM preferences</source>
        <translation>Настройки SSH TBM</translation>
    </message>
    <message>
        <location filename="preferencesdialog.ui" line="20"/>
        <source>Start SSH-TBM when system starts</source>
        <translation>Запускать SSH-TBM при старте системы</translation>
    </message>
    <message>
        <location filename="preferencesdialog.ui" line="27"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When enabled, application icon will be displayed in notification area. Also application remains active  when you close main window.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Когда включено, значок приложения будет показан в зоне нотификаций на таскбаре. Также приложение будет оставаться активным после закрытия главного окна.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="preferencesdialog.ui" line="30"/>
        <source>Enable tray/notification icon</source>
        <translation>Включить значок приложения в зоне нотификаций</translation>
    </message>
    <message>
        <location filename="preferencesdialog.ui" line="37"/>
        <source>Enable event log</source>
        <translation>Включить журнал событий</translation>
    </message>
    <message>
        <location filename="preferencesdialog.ui" line="46"/>
        <source>Interface language</source>
        <translation>Язык интерфейса</translation>
    </message>
    <message>
        <location filename="preferencesdialog.ui" line="99"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>TrayManager</name>
    <message>
        <location filename="traymanager.cpp" line="44"/>
        <location filename="traymanager.cpp" line="200"/>
        <source>Open tunnels list...</source>
        <translation>Открыть список туннелей…</translation>
    </message>
</context>
<context>
    <name>TunnelConnectionDialog</name>
    <message>
        <location filename="tunnelconnectiondialog.ui" line="17"/>
        <source>SSH connection properties</source>
        <translation>Свойства SSH-соединения</translation>
    </message>
    <message>
        <location filename="tunnelconnectiondialog.ui" line="28"/>
        <source>Tunnel type</source>
        <translation>Тип туннеля</translation>
    </message>
    <message>
        <location filename="tunnelconnectiondialog.ui" line="38"/>
        <source>Local host</source>
        <translation>Локальный хост</translation>
    </message>
    <message>
        <location filename="tunnelconnectiondialog.ui" line="68"/>
        <source>Local port</source>
        <translation>Локальный порт</translation>
    </message>
    <message>
        <location filename="tunnelconnectiondialog.ui" line="88"/>
        <source>Remote host</source>
        <translation>Внешний хост</translation>
    </message>
    <message>
        <location filename="tunnelconnectiondialog.ui" line="109"/>
        <source>Remote port</source>
        <translation>Внешний порт</translation>
    </message>
    <message>
        <location filename="tunnelconnectiondialog.cpp" line="24"/>
        <source>SOCKS proxy</source>
        <translation>SOCKS-прокси</translation>
    </message>
    <message>
        <location filename="tunnelconnectiondialog.cpp" line="25"/>
        <source>Forward local port (-L)</source>
        <translation>Редирект локального порта (ssh -L)</translation>
    </message>
    <message>
        <location filename="tunnelconnectiondialog.cpp" line="26"/>
        <source>Forward remote port (-R)</source>
        <translation>Редирект внешнего порта (ssh -R)</translation>
    </message>
    <message>
        <location filename="tunnelconnectiondialog.cpp" line="119"/>
        <location filename="tunnelconnectiondialog.cpp" line="145"/>
        <source>This field is required!</source>
        <translation>Поле обязательное!</translation>
    </message>
    <message>
        <location filename="tunnelconnectiondialog.cpp" line="126"/>
        <location filename="tunnelconnectiondialog.cpp" line="152"/>
        <source>Invalid port value, must be a number in range between 1 and 65535!</source>
        <translation>Некорректное значение порта, должно быть числом от 1 до 65535!</translation>
    </message>
    <message>
        <location filename="tunnelconnectiondialog.cpp" line="136"/>
        <source>Invalid port value, must be a number in range between 1025 and 65535!</source>
        <translation>Некорректное значение порта, должно быть числом от 1025 до 65535!</translation>
    </message>
    <message>
        <location filename="tunnelconnectiondialog.cpp" line="177"/>
        <source>Accept connections to SOCKS-proxy on address &lt;strong&gt;%1:%2&lt;/strong&gt;.</source>
        <translation>Принимать соединения к SOCKS-прокси по адресу &lt;strong&gt;%1:%2&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="tunnelconnectiondialog.cpp" line="183"/>
        <source>Forward local connections to address &lt;strong&gt;%1:%2&lt;/strong&gt; through the tunnel to remote address &lt;strong&gt;%3:%4&lt;/strong&gt;.</source>
        <translation>Редирект локальных соединений на адрес &lt;strong&gt;%1:%2&lt;/strong&gt; через туннель на внешний адрес &lt;strong&gt;%3:%4&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="tunnelconnectiondialog.cpp" line="191"/>
        <source>Forward connections from remote network to address &lt;strong&gt;%3:%4&lt;/strong&gt; through the tunnel back to address &lt;strong&gt;%1:%2&lt;/strong&gt; in local network.</source>
        <oldsource>Forward connections from remote network to address &lt;strong&gt;%1:%2&lt;/strong&gt; through the tunnel back to address &lt;strong&gt;%3:%4&lt;/strong&gt; in local network.</oldsource>
        <translation>Редирект соединений из внешней сети на адрес &lt;strong&gt;%3:%4&lt;/strong&gt; через туннель назад в локальную сеть в адрес &lt;strong&gt;%1:%2&lt;/strong&gt;.</translation>
    </message>
</context>
<context>
    <name>TunnelItemDelegate</name>
    <message>
        <location filename="tunnelitemdelegate.cpp" line="84"/>
        <source>SOCKS proxy on port %2:%1 through server %3</source>
        <translation>SOCKS-прокси на порту %2:%1 через сервер %3</translation>
    </message>
    <message>
        <location filename="tunnelitemdelegate.cpp" line="90"/>
        <source>Forward local port %2:%1 to %4:%5 through server %3</source>
        <oldsource>Forward local port %1 to %3:%4 through server %2</oldsource>
        <translation>Редирект локального порта %2:%1 на %4:%5 через сервер %3</translation>
    </message>
    <message>
        <location filename="tunnelitemdelegate.cpp" line="98"/>
        <source>Forward remote port %4:%5 to local port %2:%1 through server %3</source>
        <translation>Редирект внешнего порта %4:%5 в локальный %2:%1 через сервер %3</translation>
    </message>
    <message>
        <location filename="tunnelitemdelegate.cpp" line="112"/>
        <source>No defined connections</source>
        <translation>Нет настроенных соединений</translation>
    </message>
    <message>
        <location filename="tunnelitemdelegate.cpp" line="119"/>
        <source>(+%1 more)</source>
        <translation>(ещё %1)</translation>
    </message>
</context>
<context>
    <name>TunnelPropertiesDialog</name>
    <message>
        <location filename="tunnelpropertiesdialog.ui" line="17"/>
        <source>SSH Tunnel Properties</source>
        <translation>Свойства SSH-туннеля</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.ui" line="42"/>
        <source>Tunnel</source>
        <translation>Туннель</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.ui" line="51"/>
        <source>Display name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.ui" line="73"/>
        <source>Server port</source>
        <translation>Порт сервера</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.ui" line="148"/>
        <source>Connections</source>
        <translation>Соединения</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.ui" line="169"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.ui" line="179"/>
        <source>Edit</source>
        <translation>Править</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.ui" line="189"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.cpp" line="31"/>
        <source>SOCKS proxy on local port %1:%2</source>
        <translation>SOCKS-прокси на локальном порту %1:%2</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.cpp" line="36"/>
        <source>Forward local port %1:%2 to remote port %3:%4</source>
        <translation>Редирект локального порта %1:%2 во внешний порт %3:%4</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.cpp" line="43"/>
        <source>Forward remote port %1:%2 to local port %3:%4</source>
        <translation>Редирект внешнего порта %1:%2 в локальный порт %3:%4</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.cpp" line="92"/>
        <source>Key-based</source>
        <translation>По ключу</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.cpp" line="93"/>
        <source>Password</source>
        <translation>По паролю</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.cpp" line="124"/>
        <source>Key not selected</source>
        <translation>Ключ не выбран</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.cpp" line="135"/>
        <source>COPY: %0</source>
        <translation>КОПИЯ: %0</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.cpp" line="214"/>
        <location filename="tunnelpropertiesdialog.cpp" line="226"/>
        <location filename="tunnelpropertiesdialog.cpp" line="250"/>
        <location filename="tunnelpropertiesdialog.cpp" line="263"/>
        <source>This field is required!</source>
        <translation>Поле обязательное!</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.cpp" line="236"/>
        <source>Invalid port value, must be a number in range between 1 and 65535!</source>
        <translation>Некорректное значение порта, должно быть числом от 1 до 65535!</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.ui" line="61"/>
        <source>Server address</source>
        <translation>Адрес сервера</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.ui" line="137"/>
        <source>Notes</source>
        <translation>Заметки</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.ui" line="107"/>
        <source>Authentication</source>
        <translation>Аутентификация</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.ui" line="127"/>
        <source>User login name</source>
        <translation>Логин</translation>
    </message>
    <message>
        <location filename="tunnelpropertiesdialog.ui" line="117"/>
        <source>Select key</source>
        <translation>Выберите ключ</translation>
    </message>
</context>
</TS>
