#ifndef TUNNELPCONNECTIONDIALOG_H
#define TUNNELPCONNECTIONDIALOG_H

#include <QtWidgets>
#include "tunnelsmanager.h"


class TunnelConnectionDialog : public QDialog
{
    Q_OBJECT

public:
    TunnelConnectionDialog(const QString & guid, QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
    ~TunnelConnectionDialog();
    void setConnection(const TunnelsManager::TunnelConnection & tcc);
    TunnelsManager::TunnelConnection connection();

public slots:
    void accept();
    
private slots:
    void kindChanged(int index);
//     void authTypeChanged(int index);
    // void acceptedHandler();
//     void rejectedHandler();
//     void changeTextDataHandler(const QString &);
    void refreshHintsLabel(const QString & text);

private:
    struct Private;
    Private * p;

    // void arrangeItems();
};

#endif