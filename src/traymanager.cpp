#include "tunnelsmanager.h"
#include "traymanager.h"
#include "settings.h"
#include "mainwindow.h"

struct TrayManager::Private
{
    QApplication * app;

    QSystemTrayIcon * trayIcon;
    QMenu * menu;

    QIcon activeIcon;
    QIcon notActiveIcon;
    QIcon runningIcon;
    QIcon startingIcon;

    QList<QAction*> tunnelActions;
    
    QAction * openTunnelsAction;
    QAction *separatorAction1;
    QAction *separatorAction2;
    QAction * quitAction;
};

static TrayManager * inst = 0;

TrayManager * TrayManager::instance()
{
    if (inst == 0) {
        inst = new TrayManager();
    }

    return inst;
}

TrayManager::TrayManager(QObject * parent)
    : QObject(parent)
{
    p = new Private;

    p->menu = new QMenu();

    p->openTunnelsAction = new QAction(tr("Open tunnels list..."));
    p->separatorAction1 = new QAction();
    p->separatorAction1->setSeparator(true);
    p->separatorAction2 = new QAction();
    p->separatorAction2->setSeparator(true);
    p->quitAction = new QAction(MainWindow::tr("Quit ssh-tbm"));

    // due to https://bugreports.qt.io/browse/QTBUG-53550 we need to render SVG icon for tray
    // QSize iconSize(64, 64);
    QSize iconSize(32, 32);

    p->activeIcon = QIcon(QIcon(":/app-blue.svg").pixmap(iconSize));
    p->notActiveIcon = QIcon(QIcon(":/app-gray.svg").pixmap(iconSize));
    p->runningIcon = QIcon(QIcon(":/running.svg").pixmap(iconSize));
    p->startingIcon = QIcon(QIcon(":/starting.svg").pixmap(iconSize));

    p->trayIcon = new QSystemTrayIcon(p->notActiveIcon, this);
    p->trayIcon->setContextMenu(p->menu);

    auto tm = TunnelsManager::instance();
    connect(p->openTunnelsAction, SIGNAL(triggered()), this, SLOT(showMainWindow()));
    connect(p->quitAction, SIGNAL(triggered()), this, SLOT(quit()));
    connect(tm, SIGNAL(reloaded()), this, SLOT(reloadTunnels()));
    connect(p->trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), 
        this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
}


TrayManager::~TrayManager()
{
    delete p;
}

void TrayManager::setApplication(QApplication * a)
{
    p->app = a;
}

void TrayManager::showMainWindow()
{
    auto mw = MainWindow::instance();

    if (mw == 0) {
        return;
    }

    mw->forceShow();
}

void TrayManager::reloadTunnels()
{
    // remove all actions from menu and add them again
    // we need to do this because of bugged insertAction/insertActions method in Qt 5.9
    for (auto action : p->menu->actions()) {
        p->menu->removeAction(action);
    }

    // now delete all tunnel actions
    for (auto action : p->tunnelActions) {
        delete action;
    }
    p->tunnelActions.clear();

    // and create them again
    auto tm = TunnelsManager::instance();
    auto tunnels = tm->tunnels();

    bool isAnyTunnelRunning = false;

    p->menu->addAction(p->openTunnelsAction);
    p->menu->addAction(p->separatorAction1);
    for (auto tunnel : tunnels) {
        QAction * action = new QAction(tunnel.name);
        action->setData(tunnel.guid);
        connect(action, SIGNAL(triggered()), this, SLOT(tunnelTriggered()));
        auto status = tm->tunnelStatus(tunnel.guid);
        if (status == TunnelsManager::StatusStarting) {
            action->setIcon(p->startingIcon);
            isAnyTunnelRunning = true;
        } else if (status == TunnelsManager::StatusRunning) {
            action->setIcon(p->runningIcon);
            isAnyTunnelRunning = true;
        }
        p->tunnelActions << action;
        p->menu->addAction(action);
    }
    p->menu->addAction(p->separatorAction2);
    p->menu->addAction(p->quitAction);

    if (isAnyTunnelRunning) {
        // change tray icon to blue one
        p->trayIcon->setIcon(p->activeIcon);
    } else {
        // change to gray one
        p->trayIcon->setIcon(p->notActiveIcon);
    }
}


void TrayManager::quit()
{
    QCoreApplication::quit();
}

void TrayManager::tunnelTriggered()
{
    QAction * senderAction = qobject_cast<QAction*>(this->sender());
    if (senderAction == 0) {
        return;
    }

    auto tm = TunnelsManager::instance();
    auto guid = senderAction->data().toString();
    auto tunnel = tm->tunnel(guid);

    if (!tunnel.isValid()) {
        return;
    }

    auto status = tm->tunnelStatus(guid);
    switch (status) {
    case TunnelsManager::StatusUnknown:
    case TunnelsManager::StatusStopped:
        qDebug() << "start" << guid;
        tm->startTunnel(guid);
        break;

    case TunnelsManager::StatusStarting:
    case TunnelsManager::StatusRunning:
        qDebug() << "stop" << guid;
        tm->stopTunnel(guid);
        break;
    }
}

void TrayManager::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::Trigger) {
#ifndef Q_OS_OSX
        // hide/show main window, only for linux/windows
        auto mw = MainWindow::instance();

        if (mw == 0) {
            return;
        }
        if (mw->isVisible()) {
            mw->hide();
        } else {
            mw->forceShow();
        }
#endif
    }
}

void TrayManager::retranslateUi()
{
    p->openTunnelsAction->setText(tr("Open tunnels list..."));
    p->quitAction->setText(MainWindow::tr("Quit ssh-tbm"));
}


void TrayManager::configChanged()
{
    if (Settings::getApplicationTrayEnabled()) {
        p->trayIcon->show();
        p->app->setQuitOnLastWindowClosed(false);
    } else {
        p->trayIcon->hide();
        p->app->setQuitOnLastWindowClosed(true);
    }
}