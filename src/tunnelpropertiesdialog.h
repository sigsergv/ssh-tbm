#ifndef TUNNELPROPERTIESDIALOG_H
#define TUNNELPROPERTIESDIALOG_H

#include <QtWidgets>

class TunnelPropertiesDialog : public QDialog
{
    Q_OBJECT

public:
    TunnelPropertiesDialog(const QString & guid, const QString & copyFromGuid, QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
    ~TunnelPropertiesDialog();

private slots:
    void authTypeChanged(int index);
    void acceptedHandler();
    void rejectedHandler();
    // void changeTextDataHandler(const QString &);
    void connectionsWidgetSelectionChanged();

    void addConnection();
    void editConnection();
    void deleteConnection();

private:
    struct Private;
    Private * p;

    void arrangeItems();
    // void updateDetailsLabel();
};

#endif