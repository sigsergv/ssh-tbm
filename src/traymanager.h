#ifndef TRAYMANAGER_H
#define TRAYMANAGER_H

#include <QtWidgets>

class TrayManager : public QObject
{
    Q_OBJECT
public:
    static TrayManager * instance();
    void setApplication(QApplication * a);
    ~TrayManager();
    void retranslateUi();
    void configChanged();

private slots:
    void reloadTunnels();
    void showMainWindow();
    void quit();
    void tunnelTriggered();
    void iconActivated(QSystemTrayIcon::ActivationReason);

private:
    TrayManager(QObject * parent = 0);

    struct Private;
    Private * p;
};

#endif