#include <QtDebug>

#include "constants.h"
#include "tunnelitemdelegate.h"
#include "tunnelpropertiesdialog.h"
#include "tunnelsmanager.h"

#define ICON_PADDING 4

TunnelItemDelegate::TunnelItemDelegate(QWidget *parent)
    : QStyledItemDelegate(parent), runningIcon(":/running.svg"), startingIcon(":/starting.svg")
{

}

void TunnelItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    // QStyledItemDelegate::paint(painter, option, index);
    QPalette palette = QApplication::palette();
    QRect baseRect = option.rect;
    QBrush b;

    auto tm = TunnelsManager::instance();

    if (option.state & QStyle::State_Selected) {
        // draw selected frame
        painter->setPen(palette.color(QPalette::HighlightedText));
        b = palette.highlight();
    } else {
        // draw not-selected frame
        painter->setPen(palette.color(QPalette::Text));
        b = palette.brush(QPalette::Active, QPalette::Base);
    }
    painter->fillRect(option.rect, b);

    // QPoint textPos = baseRect.topLeft();
    // textPos.setX(baseRect.topLeft().x());
    // textPos.setY(baseRect.topLeft().y());

    auto tunnelGuid = index.data().toString();
    auto data = tm->tunnel(tunnelGuid);

    QRect textRect(baseRect);
    int h = baseRect.height() / 2;

    // draw icon for starting/running tunnel
    auto iconSize = baseRect.height() - 2 * ICON_PADDING;
    int horizOffset = 0;
    auto status = tm->tunnelStatus(tunnelGuid);

    if (status == TunnelsManager::StatusStarting || status == TunnelsManager::StatusRunning) {
        horizOffset = baseRect.height();

        QRect iconRect(baseRect);
        iconRect.adjust(ICON_PADDING, ICON_PADDING, 0, 0);
        iconRect.setBottom(iconRect.top() + iconSize);
        iconRect.setRight(iconRect.left() + iconSize);

        if (status == TunnelsManager::StatusStarting) {
            startingIcon.paint(painter, iconRect);
        } else {
            runningIcon.paint(painter, iconRect);
        }
    }

    // draw tunnel name on top half
    auto origFont = painter->font();
    auto font(origFont);
    font.setBold(true);
    painter->setFont(font);
    textRect.adjust(horizOffset + 5, 5, 0, -h);
    painter->drawText(textRect, Qt::TextSingleLine | Qt::TextDontClip,  data.name);

    // draw details on bottom half
    QString text;

    auto notesLines = data.notes.split('\n');
    auto firstNotesLine = notesLines[0];

    if (data.connections.size() > 0) {
        if (firstNotesLine.isEmpty()) {
            switch (data.connections[0].kind) {
            case TunnelsManager::TunnelConnection::SocksProxy:
                text = tr("SOCKS proxy on port %2:%1 through server %3")
                    .arg(data.connections[0].localPort)
                    .arg(data.connections[0].localHost)
                    .arg(data.serverAddress);
                break;
            case TunnelsManager::TunnelConnection::ForwardLocalPort:
                text = tr("Forward local port %2:%1 to %4:%5 through server %3")
                    .arg(data.connections[0].localPort)
                    .arg(data.connections[0].localHost)
                    .arg(data.serverAddress)
                    .arg(data.connections[0].remoteHost)
                    .arg(data.connections[0].remotePort);
                break;
            case TunnelsManager::TunnelConnection::ForwardRemotePort:
                text = tr("Forward remote port %4:%5 to local port %2:%1 through server %3")
                    .arg(data.connections[0].localPort)
                    .arg(data.connections[0].localHost)
                    .arg(data.serverAddress)
                    .arg(data.connections[0].remoteHost)
                    .arg(data.connections[0].remotePort);
                break;
            }
        } else {
            text = firstNotesLine;
        }
        font.setBold(false);
        painter->setFont(font);
    } else {
        text = tr("No defined connections");
        font.setItalic(true);
        font.setBold(false);
        painter->setFont(font);
    }

    if (data.connections.size() > 1 && firstNotesLine.isEmpty()) {
        text += " " + tr("(+%1 more)").arg(data.connections.size() - 1);
    }

    textRect = baseRect;
    textRect.adjust(horizOffset + 5, h+5, 0, 0);
    painter->drawText(textRect, Qt::TextSingleLine | Qt::TextDontClip,  text);
    painter->setFont(origFont);

    painter->setPen(QColor(190,190,190));
    painter->drawLine(baseRect.bottomLeft(), baseRect.bottomRight());
}

QSize TunnelItemDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QSize s = QStyledItemDelegate::sizeHint(option, index);
    s.setHeight(s.height() * 2 + 20);

    return s;
}

// void TunnelItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
// {

// }
