#include <QtWidgets>
#include <QtDebug>

#include "mainwindow.h"
#include "traymanager.h"
#include "tunnelsmanager.h"
#include "settings.h"

// use different tokens for RELEASE and DEBUG modes
#ifdef QT_DEBUG
#define SEM_TOKEN "3bf65896-70e6-44c6-9254-7dd51cd6c996"
#define SMEM_TOKEN "652c03a6-a81a-4598-84b9-46ab87b28594"
#else
#define SEM_TOKEN "7c7bd066-f5aa-4b51-9b61-63c88fbac6f9"
#define SMEM_TOKEN "4347a173-1529-4975-a4f0-f938fbf61c52"
#endif

int main(int argv, char *args[])
{
    QApplication app(argv, args);

    bool systemStartup = false;
    // some basic arg parsing
    for (int i=0; i<argv; i++) {
        if (QString(args[i]).compare("--startup") == 0) {
            systemStartup = true;
            break;
        }
    }

    // initialize settings handler
    QSettings * s = Settings::settings();
    if (s == 0) {
        // something bad happened
        return 1;
    }

    // data migration
    TunnelsManager::tunnelsConfigMigrationV2();

    // initialize managers
    TunnelsManager * tm = TunnelsManager::instance();
    TrayManager * tray = TrayManager::instance();

    // only one instance of the application is allowed
    QSystemSemaphore sem(SEM_TOKEN, 1, QSystemSemaphore::Create);
    sem.acquire();

#ifndef Q_OS_WIN32
    // on linux/unix shared memory is not freed upon crash
    // so if there is any trash from previous instance, clean it
    QSharedMemory nix_fix_shmem(SMEM_TOKEN);
    if(nix_fix_shmem.attach()) {
        nix_fix_shmem.detach();
    }
#endif

    QSharedMemory shmem(SMEM_TOKEN);
    bool is_running;
    if (shmem.attach()) {
        is_running = true;
    } else {
        shmem.create(1);
        is_running = false;
    }
    sem.release();

    if (is_running) {
        QMessageBox::critical(0, "critical", "Application is already running!");
        return 1;
    }
 
    // load translators
    Settings::translator();

    MainWindow * w = MainWindow::instance();

    tray->setApplication(&app);    
    
    // don't show main window if tray mode is enabled
    if (!Settings::getApplicationTrayEnabled() || !systemStartup) {
        w->show();
    }

    tray->configChanged();

    auto res = app.exec();

    // explicitly destroy tunnelsmanager because it's not being automatically destroyed
    delete tm;

    return res;
}