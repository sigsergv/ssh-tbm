#ifndef MACOS_H
#define MACOS_H

#ifdef Q_OS_MAC

void setDockIconStyle(bool hidden);
QIcon getMacosAddIcon();
QIcon getMacosRemoveIcon();
QIcon getMacosActionIcon();

#endif

#endif