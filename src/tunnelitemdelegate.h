#ifndef TUNNELITEMDELEGATE_H
#define TUNNELITEMDELEGATE_H

#include <QtWidgets>

class TunnelItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    TunnelItemDelegate(QWidget *parent = 0);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    // void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;

private:
    QIcon runningIcon;
    QIcon startingIcon;
};

#endif