rmdir /S /Q Release
mkdir Release
copy ssh-tbm.exe Release
copy ssh-askpass.exe Release
windeployqt.exe --release Release/ssh-tbm.exe
mkdir Release\bundled-ssh
copy ..\resources\ssh-binary-windows\*.dll Release\bundled-ssh\
copy ..\resources\ssh-binary-windows\ssh.exe Release\bundled-ssh\
