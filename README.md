SSH Tunnel Boring Machine
=========================

Multiplatform manager of SSH tunnel/proxies.


Features
========

* Three kinds of secure tunnels: SOCKS-proxy, local port forwarding, remote port forwarding;
* optional notification area icon with tunnels list in context menu;
* native linux and macos support;
* multiple UI languages support.

Linux
=====

You need to install `ssh-askpass` to enable GUI ssh key passphrase dialog. For example: `ksshaskpass`, `ssh-askpass`, `ssh-askpass-gnome`, `lxqt-openssh-askpass`.


Macos
=====

The project includes `mac-ssh-askpass` script taken from <https://github.com/theseal/ssh-askpass>.

Macos binary builds are available at <https://gitlab.com/sigsergv/ssh-tbm/tags>.


Artwork
=======

Additional icons (hourglass.svg) made by <https://www.freepik.com/> from <https://www.flaticon.com/> is licensed by CC 3.0 BY.

Additional icons (coal.svg) made by <https://www.flaticon.com/authors/pause08> from <https://www.flaticon.com/> is licensed by CC 3.0 BY.