Development
===========

See section "Compilation" below if you need detailed instructions about compilation process.

Build type "Debug" uses its own configuration file. For macos it's
`~/Library/Preferences/ssh-tbm/settings-debug.ini`, and for linux is
`~/.config/ssh-tbm/settings-debug.ini`. So you can safely use and develop application at the
same time.


Compilation
===========

Basic requirements
------------------

* Qt >= 6.4
* C++ compiler
* Linux/MacOS
* cmake (install from linux repository or from brew)

Also CMake configuration files explicitly require version 6.4.2 for Macos. Install it using
Qt Online Installer (<https://www.qt.io/download-qt-installer>).


Linux (debian/ubuntu)
---------------------

Install required packages:

~~~~~
sudo apt install dpkg-dev cmake debhelper dh-exec qt6-base-dev qt6-tools-dev qt6-svg-dev
~~~~~

Build instructions:

~~~~~
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Debug ..
$ cmake --build .
~~~~~

To create dpkg package use these commands (inside project directory):

~~~~~
sudo apt install fakeroot
dpkg-buildpackage -rfakeroot -b
~~~~~


Macos
-----

Desclaimer: these instructions assume that you are using official Qt 6.5.3 opensource distribution
from Qt web site. You also need to install Qt default location (`~/Qt/`). This version and path are
hardcoded in `CMakeLists.txt` file. Resulting binary is Mach-O 64-bit executable x86_64.

You can use [aqtinstall](https://github.com/miurahr/aqtinstall) instead of official installer:

~~~~
$ aqt install-qt -O ~/Qt/ mac desktop 6.5.3
~~~~

You must install XCode command line tools first:

~~~~
$ xcode-select --install
~~~~

You also need a tool `create-dmg`:

~~~~
$ brew install create-dmg
~~~~

Here are the steps to create macos bundle. First build a release version:

~~~~~
$ rm -rf build
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ cmake --build .
~~~~~

Use macdeployqt to prepare bundle (inside directory `build`):

~~~~~
$ ~/Qt/6.5.3/macos/bin/macdeployqt ssh-tbm.app
~~~~~

Check deps (we need to provide only Qt frameworks):

~~~~~
$ otool -L ssh-tbm.app/Contents/MacOS/ssh-tbm
~~~~~

Examine the output and make sure that the only external dependencies are standard system ones.

Then create DMG package using script `create-dmg`:

~~~~~
$ ../scripts/create-macos-dist
~~~~~

Workaround for debug version execution error (see <https://bugreports.qt.io/browse/QTBUG-71724>):

~~~~
export DYLD_IMAGE_SUFFIX=_debug
~~~~


Windows
-------

Disclaimer: windows build is not functional yet.

We're building x86 version for compatibility reasons, but doing it inside x86_64 environment
of Windows 10 Pro installation. We do not require `bash` or other tools except ones provided
by Qt or cmake.


Download links:

* Qt6
* cmake >= 3.15

Install Qt to default location (`C:\Qt\Qt6.4.2`), select *Qt x.y.z > MinGW a.b.c 32-bit* and
*Tools > MinGW a.b.c 32-bit*, all other components are not needed.

Install cmake using default options. 

Update environment variable PATH inside cmd.exe session:

~~~~
set PATH=%PATH%;C:\Qt\Qtx.y.x\x.y.z\mingw73_32\bin;C:\Qt\Qtx.y.z\Tools\mingw730_32\bin;C:\Program Files (x86)\CMake\bin
~~~~

Build instructions:

* mkdir build
* cd build
* cmake -G "MinGW Makefiles" ..
* cmake --build . --config Release

Use provided script `win-create-release.bat` to preare release:

~~~~
..\scripts\win-create-release.bat
~~~~

Translations
============

For linux:

~~~~~
lupdate -no-obsolete -recursive ./src -ts src/ssh-tbm-ru.ts
~~~~~

For macos:

~~~~~
~/Qt/6.4.2/clang_64/bin/lupdate -no-obsolete -recursive ./src -ts src/ssh-tbm-ru.ts
~~~~~


