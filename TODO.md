Version 2.0
===========

* (v) add hint explaining what exactly each kind of port forwarding is

Version 1.1
===========

* (v) host key verification dialog;
* (v) actual autorun support for linux;
* (v) some documentation
* (v) handle port conflicts
* (v) optional tray/notification icon, disabled by default
* (v) single and optional event log

Version 1.3
===========

* (v) new informational field for tunnel: description
* (v) rearrange/dragndrop for tunnels
* (v) open tunnel properties by double click

Version 1.4
===========

* more detailed eventlog
* clone/copy

Version x.x
===========

* auto connect on disconnect
* windows support
* more detailed eventlog
* session compression options
