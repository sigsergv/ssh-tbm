# Actors

* ssh server — software that runs on remote server and accepts connections using SSH protocol
* ssh client — software that runs on local machine and connects to ssh server using SSH protocol

Tunnel has the following properties:

Informational:

* DISPLAY_NAME

SSH server connection (we refer them as SSH_SERVER):

* SERVER_ADDRESS
* SERVER_PORT
* SSH_USERNAME
* AUTH_KEY

Tunnel properties:

* LOCAL_PORT
* LOCAL_HOST
* TUNNEL_TYPE
* REMOTE_HOST
* REMOTE_PORT

# Cases

There are few possible failures what may occur during tunnel lifetime.

## Tunnel: SOCKS proxy

Ssh client open LOCAL_PORT on LOCAL_HOST and start SOCKS-proxy server on that port.
LOCAL_PORT must be in range 1025—65535 inclusive, i.e. privileged ports are not supported.




### case TUN-SOCKS-001: bind to port failed

#### Summary

Bind to LOCAL_HOST:LOCAL_PORT failed because LOCAL_PORT is already bound to another application.

#### Steps

Start another service that binds to port LOCAL_PORT on host LOCAL_HOST (`netcat` package required):

    $ nc -l -p 5000 127.0.0.1

Configure a new tunnel as SOCKS-server and set *Local host* field to `127.0.0.1` and *Local port* field to `5000`.

Try to start server

#### Algorithm

Scan ssh process stderr and look for line: `bind: Address already in use`





### case TUN-SOCKS-002: bind to IP failed

#### Summary

LOCAL_HOST is IP address.

Bind to LOCAL_HOST:LOCAL_PORT failed because LOCAL_HOST doesn't exist as local IP address.

#### Steps

Configure a new tunnel as SOCKS-server and set *Local host* field to non-existing on local machine IP address,
for example, *10.10.10.10*.

Try to start server.

#### Algorithm

Scan ssh process stderr and look for line: `bind: Cannot assign requested address`




### case TUN-SOCKS-003: LOCAL_HOST resolve failed

#### Summary

Bind to LOCAL_HOST:LOCAL_PORT failed because LOCAL_HOST is not a proper IP address and cannot be resolved to IP address.

#### Steps

Configure a new tunnel as SOCKS-server and set *Local host* field to non-existing domain name, for example, to *domain.doesntexist*.

Try to start server.

#### Algorithm

Scan ssh process stderr and look for one of lines: 

* `channel_setup_fwd_listener_tcpip: getaddrinfo(%DOMAIN%): Name or service not known` (linux)
* `channel_setup_fwd_listener_tcpip: getaddrinfo(%DOMAIN%): nodename nor servname provided, or not known` (macos)

where `%DOMAIN%` is a LOCAL_HOST. Match line by regexp or by substring (head and tail).





### case TUN-SOCKS-SUCCESS:

Tunnel has been started.

This case is considered completed if no known failures occured.

#### Algorithm

Wait for string `channel 0: new [port listener]`.




## Tunnel: forward local port

Description: all connections to LOCAL_HOST:LOCAL_PORT are forwarded to REMOTE_HOST:REMOTE_PORT via server SSH_SERVER. 
Ssh client open port LOCAL_PORT on LOCAL_HOST and start listening. LOCAL_HOST must be a working
IP address on local machine.




### case TUN-FWD-LOC-001: bind to port failed

#### Summary

Bind to LOCAL_HOST:LOCAL_PORT failed because LOCAL_PORT is already bound to another application.

#### Steps

Start another service that binds to port LOCAL_PORT on host LOCAL_HOST (`netcat` package required):

    $ nc -l -p 5000 127.0.0.1

Configure a new tunnel as *Forward local port (-L)* and set *Local host* field to `127.0.0.1` and *Local port* field to `5000`.

Try to start server

#### Algorithm

Scan ssh process stderr and look for line: `listen: Address already in use`




### case TUN-FWD-LOC-002: bind to IP failed

#### Summary

LOCAL_HOST is IP address.

Bind to LOCAL_HOST:LOCAL_PORT failed because LOCAL_HOST doesn't exist as local IP address.

#### Steps

Configure a new tunnel as *Forward local port (-L)* and set *Local host* field to non-existing on local machine IP address,
for example, *10.10.10.10*.

Try to start server.

#### Algorithm

Scan ssh process stderr and look for line: `bind: Cannot assign requested address`





### case TUN-FWD-LOC-003: LOCAL_HOST resolve failed

#### Summary

Bind to LOCAL_HOST:LOCAL_PORT failed because LOCAL_HOST is not a proper IP address and cannot be resolved to IP address.

#### Steps

Configure a new tunnel as *Forward local port (-L)* and set *Local host* field to non-existing domain name, for example, to *domain.doesntexist*.

Try to start server.

#### Algorithm

Scan ssh process stderr and look for one of lines: 

* `channel_setup_fwd_listener_tcpip: getaddrinfo(%DOMAIN%): Name or service not known` (linux)
* `channel_setup_fwd_listener_tcpip: getaddrinfo(%DOMAIN%): nodename nor servname provided, or not known` (macos)

where `%DOMAIN%` is a LOCAL_HOST. Match line by regexp or by substring (head and tail).





### case TUN-FWD-LOC-SUCCESS: 

Tunnel has been started.

This case is considered completed if no known failures occured.

#### Algorithm

Wait for string `Local forwarding listening on %LOCAL_HOST% port %LOCAL_PORT%.`


## Tunnel: forward remote port

Description: all connections to REMOTE_HOST:REMOTE_PORT are forwarded to LOCAL_HOST:LOCAL_PORT via server SSH_SERVER.
Ssh server open port REMOTE_PORT on host REMOTE_HOST and start listening. Ssh client redirects all traffic to port
LOCAL_PORT on LOCAL_HOST. LOCAL_HOST could be any address reachable from local machine.




### case TUN-FWD-REMOTE-SUCCESS:

Tunnel has been started.

This case is considered completed if no known failures occured.

#### Algorithm

Wait for string `remote forward success for: listen REMOTE_HOST:REMOTE_PORT, connect LOCAL_HOST:LOCAL_PORT`.
