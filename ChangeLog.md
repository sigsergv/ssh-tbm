3.0
===

* migration to Qt6
* fixed tunnel properties dialog layout in linux
* fixed compile warning
* fixed debian package dependencies

2.5
===

* added support of generic SSH keys

2.4
===

* updated build instructions to compile x86_64 binaries on arm64 macos

2.3
===

* hdpi macos displays are supported now
* fixed status detection for newer openssh-client versions

2.2
===

* added new feature: Copy server entry
* fixed issue when server can be started without any connections

2.1
===

* fixed issue that just first requestsed connection was has been created

2.0
===

* it's now possible to define several tunnels for one connection
* list of tunnels in notification area menu is now updated properly
* multiple layout and text fixes

1.7
===

DATE

* new action: Restart tunnel

1.6
===

Mon Sep 30 09:29:24 2019 +0700

* new icons
* use doubleclick on list item to start/stop connection
* new target Qt version 5.11

1.5
===

Tue Jan 1 18:45:27 2019 +0700

* some minor cosmetic changes
* updated documentation
* fixed issue with tunnel deletion
* new icons from https://www.flaticon.com

1.4
===

Sun, 18 Feb 2018 19:33:34 +0700

* improvements in event log
* fixing bugs
* rewriting developer documentation
* new recommended version of qt: 5.9.4

1.3
===

Mon, 02 Oct 2017 21:37:12 +0700

* reorder/dragndrop for tunnels
* open tunnel properties by double click

1.2
===

Sun, 23 Jul 2017 11:02:21 +0700

* some layout changes
* notification area icon behaviour changed, it's now different for macos and other OSes
* fixed macos packaging script
* fixed serverPort use

1.1
===

Fri, 14 Jul 2017 19:00:13 +0700

* tray/notification icon is now optional and disabled by default
* added dialog for password authentication and macos
* added host key verification dialog
* fixed `SSH_ASKPASS` use in linux
* added autostart in linux
* eventlog is now optional (turned off by default) and consolidated
* improved process state detection and errors handling
* added separate settings file for debug mode
* added flag `--startup` to autostart configs to hide main window if application is working in tray mode
* click on notification icon now show/hide main window
* added local port forwarding
* added remote port forwarding


1.0
===

Tue, 27 Jun 2017 20:27:48 +0700

Basic features:

* create/edit/delete tunnels;
* start/stop tunnel;
* preferences: UI language and autorun support (macos only);
* basic eventlog for tunnels;
* builds for linux and macos;
* packaging for macos and debian/ubuntu;
* poor icons and other artwork.
