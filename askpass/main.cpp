#include <QtWidgets>
#include <QtDebug>

#include "mainwindow.h"

int main(int argv, char *args[])
{
    QApplication app(argv, args);

    QString prompt;
    auto c = app.arguments().count();
    if (c < 2) {
        prompt = "<empty prompt>";
        // prompt = QString::number(argv);
    } else {
        prompt = app.arguments().at(c - 1);
    }
    MainWindow w(prompt);
    w.show();

    return app.exec();
}