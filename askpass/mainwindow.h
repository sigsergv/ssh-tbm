#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDialog>

#include "ui_mainwindow.h"

class MainWindow : public QDialog
{
    Q_OBJECT
public:
    MainWindow(const QString & prompt, QWidget * parent = 0);
    ~MainWindow();

public slots:
    void accept();
    void reject();

private:
    Ui::MainWindow ui;
};

#endif