#include <QtDebug>
#include <iostream>

#include "mainwindow.h"

MainWindow::MainWindow(const QString & prompt, QWidget * parent)
    : QDialog()
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui.setupUi(this);
    ui.promptLabel->setText(prompt);
}

MainWindow::~MainWindow()
{
}

void MainWindow::accept()
{
    auto text = ui.message->text();
    // output to STDOUT
    std::cout << text.toUtf8().constData() << std::flush;
    qApp->exit(0);
}

void MainWindow::reject()
{
    qApp->exit(1);
}
